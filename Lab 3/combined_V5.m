%% start dynamixel
% import variables
% define sortmap
clear all;
setup_v5

%% default position

%initialise motor speed
for i = 1:4
    calllib('dynamixel','dxl_write_word',id(i),MOVING_SPEED,50);
end

while(1)
    close all
   % if firsttime
        %move arm to default position
        for i = [4,2,3,1]
            motor(i,speed(i),startpos(i));
            pause(0.8)
        end

        pause(4);

        %% scan for dominos

        %take photo
        photo = test_camera();

        %determine locations
        location_dominos;
        
        %initiate j
        j = 0;
        
        %kill flag firsttime
        firsttime = 0;
   % end
    
    if size(sideloc,1) == 0
        disp('Found Nothing')
        continue
    end
    
    %filter
    if j < size(sideloc,1)
            j = j + 1;
    else
        disp('No Dominos left/present')
        firsttime = 1;
        flagrestart = 1;
        continue
    end
    
    %Initialize restart flag
    flagrestart = 0;

    while any(domIDs(j,1) == -[1 2 3 4])
        if j < size(sideloc,1)
            j = j + 1;
        else
            disp('No Dominos left/present')
            firsttime = 1;
            flagrestart = 1;
            break
        end
    end
    
    %if no valid dominos present
    if flagrestart
        %look again
        continue
    end
    
    %find dominos that are real
    passedindex = (domIDs(:,1)>=0);
    passed = domIDs(passedindex,:);
    disp('Passed:');
    disp(passed');

    %declare dominos that are false
    notpassedindex = (domIDs(:,1)<0);
    notpassed = domIDs(notpassedindex,:);
    disp('Not Passed:');
    disp(notpassed');


    disp('Domino is:');
    disp(domIDs(j,:));
    
    
    
    %% sweep domino (alter orientation)
    
%     if angle(j) < 70 && angle(j) > -70    %forget about small angles off
%         disp('Time to SWEEP')
%         
%         %change arm sweep depending on angle
%         if angle(j) < 0
%             sweep_adjust = 15; % above domino sweeping
%         else
%             sweep_adjust = -15; %below domino sweeping
%         end

        %% move to start of sweep
%         [theta1,theta2] = invkin_side(loc(j,1) + sweep_adjust); %check 1 or 2
%         position = (loc(j,2)-50)*turn_scale;
%        
%         pres_pos = (calllib('dynamixel','dxl_read_word',id(1),PRESENT_POSITION));
%         motor(1,speed(1),round(position(1))-(slide_pos-pres_pos));
%         
%         display(position(1))
%         display(pres_pos)
%         display(slide_pos)
%         display(position(1)-(slide_pos-pres_pos))
%         
%         slide_pos=position(1);
%         
%         pause(1)
%         
%         for i = [4,3,2];
%             if position(i)>max_lim(i) || position(i)<min_lim(i)
%                 display(i)
%                 display(position(i))
%                 display('overlimit')
%                 
%                 pause
%             else
%                 motor(i,speed(i),position(i));
%                 pause(1)
%             end
%         end
%         pause(4)
%         
%         %move move sliding motor to sweep (loc,2)+20
%         goal_pos = (loc(j,2)+50)*turn_scale;
%         pres_pos = (calllib('dynamixel','dxl_read_word',id(1),PRESENT_POSITION));
%         motor(1,speed(1),round(goal_pos)-(slide_pos-pres_pos));
%         display(goal_pos)
%         display(pres_pos)
%         display(slide_pos)
%         display(goal_pos-(slide_pos-pres_pos))
%         
%         slide_pos=goal_pos;
%         pause(4)
%         
%         motor(2,speed(2),position(2)+50)
%         pause(2)
        
%     else

 %% pick-up domino
        disp('Time to GROPE')
  map_index = 0;      
        try
            invkin_side(sideloc(j,:));
        catch ME
            disp('INVKIN NOT WORKING')
            continue
        end
        [theta1,theta2] = invkin_side(sideloc(j,:)); %check 1 or 2
      
        
        map_index = 0;
        
        display('MAP DOMINOS')
        %Decide where to put dominos
        for i = 1:length(sortdots)
            if (domIDs(j,1)==sortdots(i,1) && domIDs(j,2)==sortdots(i,2)) || ...
                    (domIDs(j,1)==sortdots(i,2) && domIDs(j,2)==sortdots(i,1))
                map_index=i;
                break
            end
        end
        
        
        [theta1_sort,theta2_sort]=invkin_side(sortmap(map_index,:)); %check 1 or 2
        
        sort_turn=(theta1+theta2)-(theta1_sort+theta2_sort);
        %sort_turn=0;
        display(sort_turn);
        display(angle(j))
        if angle(j)>0
        mx_turn=180+(angle(j)-90);
        else
        mx_turn=180+(angle(j)+90);
        end
        display(mx_turn);
        position=[(mx_turn+sort_turn+90)*turn_scale,...
            (theta1-angle_to_zero_1)*angle_scale,...
            (theta2-angle_to_zero_2)*angle_scale,...
            claw_open]; %grabber open
        
        
        
       
        
        pres_pos = (calllib('dynamixel','dxl_read_word',id(1),PRESENT_POSITION));
%         motor(1,speed(1),round(position(1))-(slide_pos - pres_pos));

%         display(position(1))
%         display(pres_pos)
%         display(slide_pos)
%         display(position(1)-(slide_pos-pres_pos))
        slide_pos=position(1);
        pause(2)
        
        for i = [4,3,2]
            if position(i)>max_lim(i) || position(i)<min_lim(i)
                display('overlimit')
                display(i);
%                 pause
            end
%             else
                motor(i,speed(i),position(i));
                pause(0.1)
%             end
        end
        
        pause(3);
        motor(4,speed(4),claw_close);
        pause(1);
         motor(1,speed(1),position(1));

        pause(2);
%         motor(2,speed(2),position(2)+50);
%         pause(2);

       
% sideloc2=[];
% %%get rid of fake dominos
% thingy = 0;
% for a=1:size(sideloc,1)
%     if domIDs(a,1)>=0
%         sideloc2=[sideloc2;sideloc(a,:)];
%         if a<=j
%             thingy = thingy+1;
%         end  
%         
%     end
%    
% end




 %% MOTION PLANNING
xTarget = sortmap(map_index,1)
yTarget = sortmap(map_index,2)
domIDs = domIDs
whatisJ = j
pause(1)
Optimal_path = motionplanv5(1000 ,500,20, sideloc, sortmap, j, xTarget, yTarget)
Optimal_path = flipud(Optimal_path);
stepPos = [0,0]

for i=1: size(Optimal_path)
    stepPos = [Optimal_path(i,1),Optimal_path(i,2)];
        % place domino in sorted position
        [theta1,theta2]=invkin_side(stepPos); %check 1 or 2
        position=[0,...
            (theta1_sort-angle_to_zero_1)*angle_scale, ...
            (theta2_sort-angle_to_zero_2)*angle_scale,...
            claw_close]; %grabber fully closed
        
        pres_pos = (calllib('dynamixel','dxl_read_word',id(1),PRESENT_POSITION));
%         motor(1,speed(1),round(position(1))-(slide_pos-pres_pos));

%         display(position(1))
%         display(pres_pos)
%         display(slide_pos)
%         display(position(1)-(slide_pos-pres_pos))
        slide_pos=position(1);
        %calllib('dynamixel','dxl_write_word',id(1),GOAL_POSITION,position(1));
        %slide_pos=300+sortmap(i,2);
        pause(0)

        for i = [3,2]
            if position(i)>max_lim(i) || position(i)<min_lim(i)
%                 pause
                display('overlimit')
            end
           
%             else
                motor(i,40,position(i));  %usually speed(i)
                pause(0)
%             end
        end
        pause(1)
end
        
        pause(4);
        motor(4,speed(4),claw_open);
        pause(1)
        motor(1,speed(1), 180*turn_scale);
        pause(1);
%     end
end

%% terminate
calllib('dynamixel','dxl_terminate');
%unloadlibrary('dynamixel')