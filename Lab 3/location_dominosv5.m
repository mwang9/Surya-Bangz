%call domino detection code
[pix_loc,angle,domIDs] = find_dominos(photo);
angle = rad2deg(angle); %convert to degrees
domIDs = transpose(domIDs);

%pixel-to-mm (real-world) location
xloc = pix_loc(:,1).*x_scale;
yloc = pix_loc(:,2).*y_scale;

% x_offset = 97;
sideloc = [xloc,yloc]
% loc = [xloc,yloc];

% %offset
% loc(:,1) = loc(:,1) - x_offset;
% loc = fliplr(loc); % to change x and y position

%sort for highest dominos first
ysort = sideloc(:,2);
[ysort,I] = sort(ysort);

%rearrange domino positions in vector for the ysort
sideloc = sideloc(I,:);
domIDs = domIDs(I,:);
angle = angle(I);

weird = any((domIDs == 0)')';
avedots = sum(sum(domIDs(weird)))/size(domIDs,1);
weirdlocs = [sideloc(:,1).*weird,sideloc(:,2).*weird];

checkloc = sideloc;
allsame = zeros(size(sideloc,1),1);
del = ones(size(sideloc,1),1);

for i=1:size(sideloc,1)
    flag = 0;
    if allsame(i)
        flag = 1;
    end
    same = ismembertol(weirdlocs(:,1),checkloc(:,1),0.4,'ByRows',true);
    if ~flag
        same(i) = 0;
    end
    allsame = same;
end
del = logical(del);

for i = 1:size(allsame,1)
    if allsame(i)
        dots = sum(domIDs(i));
        if dots<avedots
            del(i) = 0;
        end
    end
end
del
domIDs(logical(1-del),:)
        
%rearrange domino positions in vector for the 0sort
sideloc = sideloc(del,:);
domIDs = domIDs(del,:);
angle = angle(del);
    


