function n_index = node_index(OPEN,xval,yval)
    %This function returns the index of the location of a node in the list
    %OPEN
    %
    %   Copyright 2009-2010 The MathWorks, Inc.
    i=1;
    while (OPEN(i,2) ~= xval || OPEN(i,3) ~= yval)
        %fprintf('xval: %d, yval: %d \n',xval,yval);
        i=i+1;
    end;
    n_index=i;
    
    %print location of following cell to move to
    %fprintf('xval: %d, yval: %d \n',xval,yval); 
end