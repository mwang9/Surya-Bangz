%% start dynamixel
% import variables
% define sortmap
setup

%% default position

%initialise motor speed
for i = 1:4
    calllib('dynamixel','dxl_write_word',id(i),MOVING_SPEED,40);   
end

while(1)

%move arm to default position
for i = [3,2,1,4]
    if i==1;
        pres_pos=int32(calllib('dynamixel','dxl_read_word',id(1),PRESENT_POSITION));%check command!!
        calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,startpos(i)-(slide_pos-pres_pos));
        pause(1)
        display(startpos(1))
display(pres_pos)
display(slide_pos)
display(startpos(i)-(slide_pos-pres_pos))
        slide_pos=startpos(i);
    else
        calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,startpos(i));
        pause(1)
    end
end


pause(4)


%take photo
photo = test_camera();

%Call domino detection code
[pix_loc,angle,domIDs]=find_dominos(photo)
angle=rad2deg(angle);
domIDs=transpose(domIDs);
%Scale location     (!need real world value!)
scale=570/640; %pixels to mm
yscale=320/360; %pixels to mm in y
xloc=pix_loc(:,1).*scale;
yloc=pix_loc(:,2).*yscale;
loc = [xloc,yloc];
%do offset
loc(:,1)=loc(:,1)-65;
loc=fliplr(loc); % to change x and y position

%

[angle,I]=sort(angle,'descend');

domIDs=domIDs(I,:)

loc=loc(I,:)

j = 1;
    while domIDs(j,1)==-1 || loc(j,1)>300
        j= j+1;
    end
    
 %{   
    %move arm up
    for i = 2:3
        if position(i)>max_lim(i) || position(i)<min_lim(i)
            display('overlimit')
            display(i)
            pause
        else
            calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,default(i));
        end
    end
    
    %move arm down
    for i = 3
        if position(i)>max_lim(i) || position(i)<min_lim(i)
            display('overlimit')
            display(i)
            pause
        else
            calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,150);
        end
    end
    pause(2)
    %}
    %% sweeping for orientation
    
    if angle(j) <80 && angle(j)>-80    %forget about small angles off
        display('SWEEPING')
        
        %change arm sweep depending on angle
        if angle(j)<0
            sweep_adjust=6; % above domino sweeping
        else
            sweep_adjust=-6; %below domino sweeping
        end
        
        
        %(!1 or 2? x or y!)
        
        %% move to start of sweep
        
        [theta1,theta2]=invkin(loc(j,1)+sweep_adjust); %check 1 or 2
        position=[(loc(j,2)-30)/(slide_motor_rad)*turn_scale*180/3.14,(theta1-angle_to_zero_1)*angle_scale, (theta2-angle_to_zero_2)*angle_scale,claw_fclose] %maybe adjust claw-close to actually closed claw
        
        % move sliding motor to start of sweep (loc,2)-20
        pres_pos=(calllib('dynamixel','dxl_read_word',id(1),PRESENT_POSITION))
        calllib('dynamixel','dxl_write_word',id(1),GOAL_POSITION,round(position(1))-(slide_pos-pres_pos));
       display(position(1))
display(pres_pos)
display(slide_pos)
display(position(1)-(slide_pos-pres_pos))
        
        slide_pos=position(1);
        
        pause(5)
        %calllib('dynamixel','dxl_write_word',id(1),GOAL_POSITION,position(1));
        %slide_pos=loc(j,2)-30;
        
        
        for i = [4,3,2];
            if position(i)>max_lim(i) || position(i)<min_lim(i)
                display(i)
                display(position(i))
                display('overlimit')
                
                pause
            else
                calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,position(i));
                pause(2)
            end
        end
        pause(3)
        %move move sliding motor to sweep (loc,2)+20
        goal_pos=(loc(j,2)+30)/slide_motor_rad*turn_scale*180/3.14;
        pres_pos=calllib('dynamixel','dxl_read_word',id(1),PRESENT_POSITION)
        calllib('dynamixel','dxl_write_word',id(1),GOAL_POSITION,round(goal_pos)-(slide_pos-pres_pos));
             display(goal_pos)
display(pres_pos)
display(slide_pos)
display(goal_pos-(slide_pos-pres_pos))
        
        slide_pos=goal_pos;
        pause(5)
        %calllib('dynamixel','dxl_write_word',id(1),GOAL_POSITION,(loc(j,2)+30)/slide_motor_rad*turn_scale*180/3.14);
        %slide_pos=loc(j,2)+30;
        
        %{
%move back to default position
 for i = 2:4
    calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,default1(i));
    pause(4)
 end
        %}
        calllib('dynamixel','dxl_write_word',id(2),GOAL_POSITION,position(2)+50);
        pause(2)
        
    end
    %%
    %grab domino
    display('Grabbing')
    [theta1,theta2]=invkin(loc(j,1)); %check 1 or 2
    position=[(loc(j,2))/slide_motor_rad*turn_scale*180/3.14,(theta1-angle_to_zero_1)*angle_scale, (theta2-angle_to_zero_2)*angle_scale,claw_open]; %grabber open
    
    
    pres_pos=calllib('dynamixel','dxl_read_word',id(1),PRESENT_POSITION)
    calllib('dynamixel','dxl_write_word',id(1),GOAL_POSITION,round(position(1))-(slide_pos-pres_pos));
      display(position(1))
display(pres_pos)
display(slide_pos)
display(position(1)-(slide_pos-pres_pos))
    slide_pos=position(1);
    pause(5)
    %calllib('dynamixel','dxl_write_word',id(1),GOAL_POSITION,position(1));
    %slide_pos=loc(j,2);
    
    for i = [4,3,2]
        if position(i)>max_lim(i) || position(i)<min_lim(i)
            display('overlimit')
            display(i);
            pause
        else
            calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,position(i));
            pause(2)
        end
    end
    pause(2);
    calllib('dynamixel','dxl_write_word',id(4),GOAL_POSITION,claw_close);%close grabber
    pause(2)
    
    calllib('dynamixel','dxl_write_word',id(2),GOAL_POSITION,position(2)+50);
    pause(2)
    map_index = 0;
    display('MAP DOMINOS')
    %Decide where to put dominos
    for i=1:length(sortdots)
        if (domIDs(j,1)==sortdots(i,1) && domIDs(j,2)==sortdots(i,2)) || ...
                (domIDs(j,1)==sortdots(i,2) && domIDs(j,2)==sortdots(i,1))
            map_index=i;
            break
        end
    end
    
    % place domino in sorted position
    [theta1,theta2]=invkin(sortmap(map_index,1)); %check 1 or 2
    position=[((sortmap(map_index,2)))/slide_motor_rad*turn_scale*180/pi,...
        (theta1-angle_to_zero_1)*angle_scale, ...
        (theta2-angle_to_zero_2)*angle_scale,...
        claw_close]; %grabber open
    
    
    pres_pos=calllib('dynamixel','dxl_read_word',id(1),PRESENT_POSITION)
    calllib('dynamixel','dxl_write_word',id(1),...
        GOAL_POSITION,round(position(1))-(slide_pos-pres_pos));
       display(position(1))
display(pres_pos)
display(slide_pos)
display(position(1)-(slide_pos-pres_pos))
    slide_pos=position(1);
    %calllib('dynamixel','dxl_write_word',id(1),GOAL_POSITION,position(1));
    %slide_pos=300+sortmap(i,2);
    pause(2)
%     last_pos = pres_pos;
%     pres_pos=calllib('dynamixel','dxl_read_word',id(1),PRESENT_POSITION)
%     dist = abs(pres_pos - last_pos)/4.2441;
%     disp(sprintf('dist: %d',dist));
    for i = [3,2]
        if position(i)>max_lim(i) || position(i)<min_lim(i)
            pause
            display('overlimit')
        else
            calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,position(i));
            pause(2)
        end
    end
    
    calllib('dynamixel','dxl_write_word',id(4),GOAL_POSITION,position(4));
    pause(2)
    calllib('dynamixel','dxl_write_word',id(4),GOAL_POSITION,claw_open);%open grabber
    pause(2)
    
end

calllib('dynamixel','dxl_terminate');
