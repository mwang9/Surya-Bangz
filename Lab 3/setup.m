%% Top Level
clc
clear all
close all

%% Start dynamixel 

%import library and functions
loadlibrary('dynamixel','dynamixel.h');
libfunctions('dynamixel');

%define port & baud
DEFAULT_PORTNUM = 4;  %COM8
DEFAULT_BAUDNUM = 1;  %1Mbps

%open device
res = calllib('dynamixel','dxl_initialize',DEFAULT_PORTNUM,DEFAULT_BAUDNUM);

%% Motor control variables

id = 1:4;
GOAL_POSITION = 30; %target position
MOVING_SPEED = 32; %rotation speed
%TORQUE_LIMIT = 34;
PRESENT_POSITION = 36; %current position
%PRESENT_SPEED = 38; %current speed
PRESENT_LOAD = 40;
MOVING = 46; %stationary = 0; moving = 1

%% World space variables

%domino length/width
leng = 50.8;
width = 25.4;

slide_motor_rad = 14.5; %drawshaft radius

<<<<<<< HEAD
angle_to_zero_1 = -64; %motor angle to inverse
=======
angle_to_zero_1 = -64+90; %motor angle to inverse
>>>>>>> rupert
angle_to_zero_2 = -248;

angle_scale = 1023/300;
diam = 29; %variable
%turn_scale = 4095/(pi*diam)
 turn_scale = 4095/360;

%position ranges
<<<<<<< HEAD
min_lim = [0,142,25,400];  %physical limit stopper
max_lim = [20000,600,990,825];
claw_fclose = 820;
claw_close = 760;
claw_open = 710;

default = [16000,404,600,claw_close]; %make sure slide motor is in right position
startpos = [0,600,600,claw_close]; %make sure slide motor is in right position
slide_pos = 0; %where the shaft should be
speed = [50,50,50,50];
position = default;
=======
min_lim = [0,0,325,0];  %physical limit stopper
max_lim = [inf,9000,9000,9000];
claw_fclose = 670;
claw_close = 653;
claw_open = 590;

% default = [0,404,600,claw_close]; %make sure slide motor is in right position
startpos = [2048,500,400,claw_open]; %make sure slide motor is in right position
slide_pos = 0; %where the shaft should be
speed = [35,60,60,80];
% position = default;

%% Camera
IAMRUNNINGSETUP =1
%pixel-to-mm scaling
x_scale = 620/640;
y_scale = 335/360;

% disp(sprintf('Time to sheep'));
>>>>>>> rupert

%% Sorting
separation_width = 80;
separation_length = 40;

%% Create sortmap for domino 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%FIRST SORT MAP
offsetx = 50; %mm
offsety = 400; %mm originally 400
sortmap = zeros(2,14); %set up map
counter = 1;
for i=1:14
    for j = 1:2
        x = i*separation_length + offsetx;
        y = -(offsety - (j*separation_width)) + 335;
%         if (i>7)
%             y = y+50;
%         end
        sortmap(counter,1) = x;
        sortmap(counter,2) = y;
        counter = counter + 1;
    end  
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%SECOND SORTMAP
% %% Create sortmap for domino 
% %offsetx = 110;%50; %mm
% %offsety = -10;%360; %mm
% 
% sortmap = zeros(28,2); %set up map
% counter = 1;
% 
% totaldeg = 60; % total deg pattern
% dominoes = 14; % dominoes sorted
% deltadeg0 = totaldeg/dominoes;
% deltadeg1 = totaldeg/dominoes;
% deltadeg2 = totaldeg/dominoes;
% 
% R1 = 500; %Radius first row from first pivo point in mm
% R2 = 400; %Radius second row from first pivo point in mm
% y0 = 300; %Y-Startpos
% Y_base = -42;
% alpha0 = asind(y0/R1);
% X_base = -480;
% 
% for i= 1:14;
%     
%             x = -1*R1*abs(cosd(alpha0 + deltadeg1)) + X_base + 900;
%             y = (-R1*abs(sind(alpha0 + deltadeg1)) + Y_base +335 -offsety + 650);
%             sortmap(counter,1) = x;
%             sortmap(counter,2) = y;
%             counter = counter + 1;
% 
%             deltadeg1=deltadeg1+deltadeg0; 
% end
% 
% for i= 15:28;
%     
%             x = -1*R2*abs(cosd(alpha0 + deltadeg2)) + X_base + 900;
%             y = (-R2*abs(sind(alpha0 + deltadeg2)) + Y_base +335 -offsety + 650);
%             sortmap(counter,1) = x;
%             sortmap(counter,2) = y;
%             counter = counter + 1;
% 
%             deltadeg2=deltadeg2+deltadeg0; 
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%import sortmap variables
%sortmap = [x_scale*sortmap(:,1), y_scale*sortmap(:,2)];

%import domino number list
<<<<<<< HEAD
sortdots=[0 0 ; 1 0; 1 1; 2 0; 2 1; 2 2 ; 3 0;3 1;3 2;3 3;4 0;4 1;4 2;4 3;4 4;
    5 0;5 1; 5 2; 5 3; 5 4; 5 5; 6 0;6 1;6 2;6 3;6 4;6 5;6 6];
%domino_list

%% Camera

%pixel-to-mm scaling
x_scale = 610/640;
y_scale = 335/360;
=======
sortdots=[0 0 ;4 4; 1 0; 5 0;1 1; 5 1;2 0;5 2; 2 1;5 3; 2 2 ;5 4; 3 0; 5 5;3 1;6 0;3 2;6 1;3 3; 6 2;4 0; 6 3;4 1;6 4;4 2; 6 5; 4 3;6 6]
%domino_list
>>>>>>> rupert

%% Flags
firsttime = 1;
flaglast = 0;
