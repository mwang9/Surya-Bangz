separation_width = 30;
separation_length = 63;

%% Create sortmap for domino 
offsetx = 110;%50; %mm
offsety = -10;%360; %mm
sortmap = zeros(28,2); %set up map
counter = 1;
for i=1:14
    for j = 1:2
        x = i*separation_length + offsetx;
        y = -(offsety - (j*separation_width));% + 335;
        sortmap(counter,1) = x;
        sortmap(counter,2) = y;
        counter = counter + 1;
    end  
end

step = 10;
sortmap(2,2) = sortmap(1,2)+ 3 * step;
sortmap(4,2) = sortmap(1,2)+ 4 * step;
sortmap(6,2) = sortmap(1,2)+ 5 * step;
sortmap(8,2) = sortmap(1,2)+ 6 * step;
sortmap(10,2) = sortmap(1,2)+ 7 * step;
sortmap(12,2) = sortmap(1,2)+ 8 * step;
sortmap(14,2) = sortmap(1,2)+ 9 * step;
sortmap(16,2) = sortmap(1,2)+ 10 * step;
sortmap(18,2) = sortmap(1,2)+ 11 * step;
sortmap(20,2) = sortmap(1,2)+ 12 * step;
sortmap(22,2) = sortmap(1,2)+ 11 * step;
sortmap(24,2) = sortmap(1,2)+ 10 * step;
sortmap(26,2) = sortmap(1,2)+ 9 * step;
sortmap(28,2) = sortmap(1,2)+ 8 * step;

sortmap(3,2) = sortmap(1,2)+ 1 * step;
sortmap(5,2) = sortmap(1,2)+ 2 * step;
sortmap(7,2) = sortmap(1,2)+ 3 * step;
sortmap(9,2) = sortmap(1,2)+ 4 * step;
sortmap(11,2) = sortmap(1,2)+ 5 * step;
sortmap(13,2) = sortmap(1,2)+ 6 * step;
sortmap(15,2) = sortmap(1,2)+ 7 * step;
sortmap(17,2) = sortmap(1,2)+ 8 * step;
sortmap(19,2) = sortmap(1,2)+ 9 * step;
sortmap(21,2) = sortmap(1,2)+ 8 * step;
sortmap(23,2) = sortmap(1,2)+ 7 * step;
sortmap(25,2) = sortmap(1,2)+ 6 * step;
sortmap(27,2) = sortmap(1,2)+ 5 * step;


%%
%{
sortmap=[separation_length*1 separation_width*10 ;
    separation_length*1 separation_width*9 ;
    separation_length*1 separation_width*8;
    separation_length*1 separation_width*7 ;
    separation_length*1 separation_width*6;
    separation_length*1 separation_width*5 ;
    separation_length*1 separation_width*4;
    separation_length*1 separation_width*3 ;
    separation_length*1 separation_width*2;
    separation_length*1 separation_width*1 ;
    separation_length*2 separation_width*10 ;
    separation_length*2 separation_width*9 ;
    separation_length*2 separation_width*8;
    separation_length*2 separation_width*7 ;
    separation_length*2 separation_width*6;
    separation_length*2 separation_width*5 ;
    separation_length*2 separation_width*4;
    separation_length*2 separation_width*3 ;
    separation_length*2 separation_width*2;
    separation_length*2 separation_width*1 ;
    separation_length*3 separation_width*10 ;
    separation_length*3 separation_width*9 ;
    separation_length*3 separation_width*8;
    separation_length*3 separation_width*7 ;
    separation_length*3 separation_width*6;
    separation_length*3 separation_width*5 ;
    separation_length*3 separation_width*4;
    separation_length*3 separation_width*3 ];
%}