
close all
clear all
clc

%separation_width = 30;
%separation_length = 63;



%% Create sortmap for domino 
%offsetx = 110;%50; %mm
%offsety = -10;%360; %mm

sortmap = zeros(28,2); %set up map
counter = 1;

totaldeg = 60; % total deg pattern
dominoes = 14; %amount dominoes sorted
deltadeg0 = totaldeg/dominoes;
deltadeg1 = totaldeg/dominoes;
deltadeg2 = totaldeg/dominoes;

R1 = 500; %Radius first row from first pivo point in mm
R2 = 400; %Radius second row from first pivo point in mm
y0 = 300; %Y-Startpos
Y_base = -42;
alpha0 = asind(y0/R1);
X_base = -480;

for i= 1:14;
    
            x = -1*R1*abs(cosd(alpha0 + deltadeg1)) + X_base + 900;
            y = R1*abs(sind(alpha0 + deltadeg1)) + Y_base;
            sortmap(counter,1) = x;
            sortmap(counter,2) = y;
            counter = counter + 1;

            deltadeg1=deltadeg1+deltadeg0; 
end

for i= 15:28;
    
            x = -1*R2*abs(cosd(alpha0 + deltadeg2)) + X_base + 900;
            y = R2*abs(sind(alpha0 + deltadeg2)) + Y_base;
            sortmap(counter,1) = x;
            sortmap(counter,2) = y;
            counter = counter + 1;

            deltadeg2=deltadeg2+deltadeg0; 
end

% X_cheat = 30;
% 
% sortmap(11,1) = sortmap(11,1)+20;
% sortmap(12,1) = sortmap(11,1)+20;
% sortmap(13,1) = sortmap(11,1)+20;
% sortmap(14,1) = sortmap(11,1)+20;

% z = 11;
% for z = 11:15;
% 
%     sortmap(z,1) = sortmap(11,1)+ X_cheat;
%     z=z+1;
% end

%%
%{
sortmap=[separation_length*1 separation_width*10 ;
    separation_length*1 separation_width*9 ;
    separation_length*1 separation_width*8;
    separation_length*1 separation_width*7 ;
    separation_length*1 separation_width*6;
    separation_length*1 separation_width*5 ;
    separation_length*1 separation_width*4;
    separation_length*1 separation_width*3 ;
    separation_length*1 separation_width*2;
    separation_length*1 separation_width*1 ;
    separation_length*2 separation_width*10 ;
    separation_length*2 separation_width*9 ;
    separation_length*2 separation_width*8;
    separation_length*2 separation_width*7 ;
    separation_length*2 separation_width*6;
    separation_length*2 separation_width*5 ;
    separation_length*2 separation_width*4;
    separation_length*2 separation_width*3 ;
    separation_length*2 separation_width*2;
    separation_length*2 separation_width*1 ;
    separation_length*3 separation_width*10 ;
    separation_length*3 separation_width*9 ;
    separation_length*3 separation_width*8;
    separation_length*3 separation_width*7 ;
    separation_length*3 separation_width*6;
    separation_length*3 separation_width*5 ;
    separation_length*3 separation_width*4;
    separation_length*3 separation_width*3 ];
%}