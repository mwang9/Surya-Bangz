function [counter] = motor(i,speed,torque,steps,counter)

id = 1:4;
GOAL_POSITION = 30; %target position
MOVING_SPEED = 32; %rotation speed
TORQUE_LIMIT = 34;
PRESENT_POSITION = 36; %current position
%PRESENT_SPEED = 38; %current speed
PRESENT_LOAD = 40;
MOVING = 46; %stationary = 0; moving = 1

position = int32(calllib('dynamixel','dxl_read_word',id(i),PRESENT_POSITION));
fprintf('Before pos: %d \n',position);
calllib('dynamixel','dxl_write_word',id(i),MOVING_SPEED,speed);
calllib('dynamixel','dxl_write_word',id(i),TORQUE_LIMIT,torque);
calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,steps);

<<<<<<< HEAD
<<<<<<< HEAD
=======
pause(0.1);
%check if moving
flag = calllib('dynamixel','dxl_read_byte',id(i),MOVING);
if flag == false
    disp('NOT MOVING');
    DEFAULT_PORTNUM = 4;  %COM8
DEFAULT_BAUDNUM = 1;  %1Mbps
    res = calllib('dynamixel','dxl_initialize',DEFAULT_PORTNUM,DEFAULT_BAUDNUM);
    calllib('dynamixel','dxl_write_word',id(i),MOVING_SPEED,speed);
    calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,steps);
end
>>>>>>> rupert
=======
move = calllib('dynamixel','dxl_read_byte',id(i),MOVING);
fprintf('Move: %d \n',move);

if move
    counter = counter + 1;
else
    disp('Did not move');
    calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,steps);
end

%fprintf('Move: %d \n',move);
%track = [];
%i = 1;
pos0 = 0;

while move
    move = calllib('dynamixel','dxl_read_byte',id(i),MOVING);
    pos1 = int32(calllib('dynamixel','dxl_read_word',id(i),PRESENT_POSITION));
    if pos1 == pos0
        disp('STUCK');
        calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,steps);
    else
        pos0 = pos1;
    fprintf('Current pos: %d \n',pos1);
    %track(i) = move;
    disp(move);
    if move == false
        disp('Done moving');
        break
    end
    %i = i + 1;
end

%fprintf('Counter: %d \n',counter);

>>>>>>> 3b619b601d8bf9aa342c654f2280b0eec151bc9f
end

%{
function [slide_pos] = motor(i,convert,dist)

% if you already know the steps, set convert to false
% convert: false/true
% convert is always false for motor 4

pres_pos = (calllib('dynamixel','dxl_read_word',id(i),PRESENT_POSITION))

converter = [dist*turn_scale - (slide_pos-pres_pos)
    (theta1-angle_to_zero_1)*angle_scale
    (theta2-angle_to_zero_2)*angle_scale
    claw_fclose];

if convert
    steps = converter(i);
else
    steps = dist;
end

if steps > max_lim(i) || steps < min_lim(i)
    fprintf('OVERLIMIT: %d \n',i);
    pause
end

calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,steps);

%motor 1 ONLY
%display positions
if i == 1
    pause(3);
    fprintf('Start Pos: %d \n',startpos(i));
    fprintf('Present Pos: %d \n',pres_pos);
    fprintf('Pos: %d \n',position(i));
    fprintf('Difference: %d \n',position(i)-(slide_pos-pres_pos));
    fprintf('Slide Pos: %d \n',slide_pos);
    slide_pos = startpos(i);
    fprintf('Slide Pos: %d \n',slide_pos);
end

end
%}
