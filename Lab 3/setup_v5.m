%% Top Level
clc
clear all
close all

%% Start dynamixel 

%import library and functions
loadlibrary('dynamixel','dynamixel.h');
libfunctions('dynamixel');

%define port & baud
DEFAULT_PORTNUM = 4;  %COM8
DEFAULT_BAUDNUM = 1;  %1Mbps

%open device
res = calllib('dynamixel','dxl_initialize',DEFAULT_PORTNUM,DEFAULT_BAUDNUM);

%% Motor control variables

id = 1:4;
GOAL_POSITION = 30; %target position
MOVING_SPEED = 32; %rotation speed
%TORQUE_LIMIT = 34;
PRESENT_POSITION = 36; %current position
%PRESENT_SPEED = 38; %current speed
PRESENT_LOAD = 40;
MOVING = 46; %stationary = 0; moving = 1

%% World space variables

%domino length/width
leng = 50.8;
width = 25.4;

slide_motor_rad = 14.5; %drawshaft radius

angle_to_zero_1 = -64+90; %motor angle to inverse
angle_to_zero_2 = -248;

angle_scale = 1023/300;
diam = 29; %variable
%turn_scale = 4095/(pi*diam)
 turn_scale = 4095/360;

%position ranges
min_lim = [0,0,325,0];  %physical limit stopper
max_lim = [inf,9000,9000,9000];
claw_fclose = 670;
claw_close = 649;
claw_open = 590;

% default = [0,404,600,claw_close]; %make sure slide motor is in right position
startpos = [2048,500,400,claw_open]; %make sure slide motor is in right position
slide_pos = 0; %where the shaft should be
speed = [35,60,60,80];
% position = default;

%% Camera
IAMRUNNINGSETUP =1
%pixel-to-mm scaling
x_scale = 620/640;
y_scale = 335/360;

% disp(sprintf('Time to sheep'));

%% Sorting
separation_width = 40;
separation_length = 63;

%% Create sortmap for domino 
offsetx = 110;%50; %mm
offsety = -10;%360; %mm
sortmap = zeros(28,2); %set up map
counter = 1;
for i=1:14
    for j = 1:2
        x = i*separation_length + offsetx;
        y = -(offsety - (j*separation_width));% + 335;
        sortmap(counter,1) = x;
        sortmap(counter,2) = y;
        counter = counter + 1;
    end  
end

step = 10;
sortmap(2,2) = sortmap(1,2)+ 5 * step;
sortmap(4,2) = sortmap(1,2)+ 6 * step;
sortmap(6,2) = sortmap(1,2)+ 7 * step;
sortmap(8,2) = sortmap(1,2)+ 8 * step;
sortmap(10,2) = sortmap(1,2)+ 9 * step;
sortmap(12,2) = sortmap(1,2)+ 10 * step;
sortmap(14,2) = sortmap(1,2)+ 11 * step;
sortmap(16,2) = sortmap(1,2)+ 12 * step;
sortmap(18,2) = sortmap(1,2)+ 13 * step;
sortmap(20,2) = sortmap(1,2)+ 14 * step;
sortmap(22,2) = sortmap(1,2)+ 13 * step;
sortmap(24,2) = sortmap(1,2)+ 12 * step;
sortmap(26,2) = sortmap(1,2)+ 11 * step;

sortmap(3,2) = sortmap(1,2)+ 1 * step;
sortmap(5,2) = sortmap(1,2)+ 2 * step;
sortmap(7,2) = sortmap(1,2)+ 3 * step;
sortmap(9,2) = sortmap(1,2)+ 4 * step;
sortmap(11,2) = sortmap(1,2)+ 5 * step;
sortmap(13,2) = sortmap(1,2)+ 6 * step;
sortmap(15,2) = sortmap(1,2)+ 7 * step;
sortmap(17,2) = sortmap(1,2)+ 8 * step;
sortmap(19,2) = sortmap(1,2)+ 9 * step;
sortmap(21,2) = sortmap(1,2)+ 8 * step;
sortmap(23,2) = sortmap(1,2)+ 7 * step;
sortmap(25,2) = sortmap(1,2)+ 6 * step;
sortmap(27,2) = sortmap(1,2)+ 5 * step;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%SECOND SORTMAP
% %% Create sortmap for domino 
% %offsetx = 110;%50; %mm
% %offsety = -10;%360; %mm
% 
% sortmap = zeros(28,2); %set up map
% counter = 1;
% 
% totaldeg = 60; % total deg pattern
% dominoes = 14; % dominoes sorted
% deltadeg0 = totaldeg/dominoes;
% deltadeg1 = totaldeg/dominoes;
% deltadeg2 = totaldeg/dominoes;
% 
% R1 = 500; %Radius first row from first pivo point in mm
% R2 = 400; %Radius second row from first pivo point in mm
% y0 = 300; %Y-Startpos
% Y_base = -42;
% alpha0 = asind(y0/R1);
% X_base = -480;
% 
% for i= 1:14;
%     
%             x = -1*R1*abs(cosd(alpha0 + deltadeg1)) + X_base + 900;
%             y = (-R1*abs(sind(alpha0 + deltadeg1)) + Y_base +335 -offsety + 650);
%             sortmap(counter,1) = x;
%             sortmap(counter,2) = y;
%             counter = counter + 1;
% 
%             deltadeg1=deltadeg1+deltadeg0; 
% end
% 
% for i= 15:28;
%     
%             x = -1*R2*abs(cosd(alpha0 + deltadeg2)) + X_base + 900;
%             y = (-R2*abs(sind(alpha0 + deltadeg2)) + Y_base +335 -offsety + 650);
%             sortmap(counter,1) = x;
%             sortmap(counter,2) = y;
%             counter = counter + 1;
% 
%             deltadeg2=deltadeg2+deltadeg0; 
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%import sortmap variables
%sortmap = [x_scale*sortmap(:,1), y_scale*sortmap(:,2)];

%import domino number list
sortdots=[0 0 ;4 4; 1 0; 5 0;1 1; 5 1;2 0;5 2; 2 1;5 3; 2 2 ;5 4; 3 0; 5 5;3 1;6 0;3 2;6 1;3 3; 6 2;4 0; 6 3;4 1;6 4;4 2; 6 5; 4 3;6 6]
%domino_list

%% Flags
firsttime = 1;
