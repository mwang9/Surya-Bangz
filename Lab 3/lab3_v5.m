%Top Level
clc
clear all
close all

%% import library and functions
loadlibrary('dynamixel','dynamixel.h')
libfunctions('dynamixel')


%define port & baud
DEFAULT_PORTNUM = 4;  %COM8
DEFAULT_BAUDNUM = 1;  %1Mbps

%open device
res = calllib('dynamixel','dxl_initialize',DEFAULT_PORTNUM,DEFAULT_BAUDNUM);

%assign control variables
id = 1:4;
GOAL_POSITION = 30;
MOVING_SPEED = 32;
TORQUE_LIMIT = 34;
PRESENT_POSITION = 36;
PRESENT_SPEED = 38;
MOVING = 46;


%real world variables
leng=50.8; %length of dom
width=25.4;
seperation_width=30; %sorting length to give seperation
seperation_length=63;
slide_motor_rad=14.5;
angle_to_zero_1=-64; %mator angle to inverse
angle_to_zero_2=-248;
%angle_to_zero_1=-65; %mator angle to inverse
%angle_to_zero_2=-253;

angle_scale=1023/300;
%diam = 29; %variable
% turn_scale = 4000/(pi*diam)
turn_scale = 4095/360;
claw_fclose=820;
claw_close=760;
claw_open=710;
default = [16000,404,600,claw_close]; %make sure slide motor is in right position
startpos = [0,600,600,claw_close]; %make sure slide motor is in right position
min_lim=[0,142,25,400];  %physical limit stopper
max_lim=[20000,600,990,825];
speed=[50,50,50,50];
position=default;
slide_pos=0;


%%
%Domino sorting matrix



%Create sortmap for domino 
offsetx = 50;%5cm
offsety = 360; %32cm
sortmap = zeros(24,2);%set up map
counter = 1;
for i=1:7
    for j = 1:4
        x = i*seperation_length+offsetx;
        y = -(offsety-(j*seperation_width))+335;
        sortmap(counter,1)= x;
        sortmap(counter,2)= y;
        counter=counter+1;
    end  
end

%Domino sorting matrix
sortdots=[0 0 ; 1 0; 1 1; 2 0; 2 1; 2 2 ; 3 0;3 1;3 2;3 3;4 0;4 1;4 2;4 3;4 4;
    5 0;5 1; 5 2; 5 3; 5 4; 5 5; 6 0;6 1;6 2;6 3;6 4;6 5;6 6];






%initialise motor speed
for i = 1:4
    calllib('dynamixel','dxl_write_word',id(i),MOVING_SPEED,40);
    %calllib('dynamixel','dxl_write_word',id(i),TORQUE_LIMIT,1023);
    
end

while(1)

%move arm to default position
for i = [2,3,4]
    if i==1;
        pres_pos=int32(calllib('dynamixel','dxl_read_word',id(1),PRESENT_POSITION));%check command!!
            calllib('dynamixel','dxl_write_word',id(i),MOVING_SPEED,speed(i));

        calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,startpos(i)-(slide_pos-pres_pos));
        pause(1)
        display(startpos(1))
display(pres_pos)
display(slide_pos)
display(startpos(i)-(slide_pos-pres_pos))
        slide_pos=startpos(i);
    else
            calllib('dynamixel','dxl_write_word',id(i),MOVING_SPEED,speed(i));

        calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,startpos(i));
        pause(1)
    end
end


pause(4)


%take photo
photo = test_camera();

%Call domino detection code
[pix_loc,angle,domIDs]=find_dominos(photo)
angle=rad2deg(angle);
domIDs=transpose(domIDs);
%Scale location     (!need real world value!)
scale=610/640; %pixels to mm
yscale=335/360; %pixels to mm in y

xloc=pix_loc(:,1).*scale;
yloc=pix_loc(:,2).*yscale;
xoffset=97;
loc = [xloc,yloc];
%do offset
loc(:,1)=loc(:,1)-xoffset;
loc=fliplr(loc); % to change x and y position

%

[angle,I]=sort(angle,'descend');

domIDs=domIDs(I,:)

loc=loc(I,:)

j = 1;
while domIDs(j,1)==-1
    if j<size(loc,1)
        j= j+1;
    else
        break
    end
end
    
    display('domino is:')
    display(domIDs(j,:));
 %{   
    %move arm up
    for i = 2:3
        if position(i)>max_lim(i) || position(i)<min_lim(i)
            display('overlimit')
            display(i)
            pause
        else
            calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,default(i));
        end
    end
    
    %move arm down
    for i = 3
        if position(i)>max_lim(i) || position(i)<min_lim(i)
            display('overlimit')
            display(i)
            pause
        else
            calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,150);
        end
    end
    pause(2)
    %}
    %% sweeping for orientation
    
    if angle(j) <70 && angle(j)>-70    %forget about small angles off
        display('SWEEPING')
        
        %change arm sweep depending on angle
        if angle(j)<0
            sweep_adjust=15; % above domino sweeping
        else
            sweep_adjust=-15; %below domino sweeping
        end
        
        
        %(!1 or 2? x or y!)
        
        %% move to start of sweep
        
        [theta1,theta2]=invkin(loc(j,1)+sweep_adjust); %check 1 or 2
        position=[(loc(j,2)-50)/(slide_motor_rad)*turn_scale*180/3.14,(theta1-angle_to_zero_1)*angle_scale, (theta2-angle_to_zero_2)*angle_scale,claw_fclose] %maybe adjust claw-close to actually closed claw
        
        % move sliding motor to start of sweep (loc,2)-20
        pres_pos=(calllib('dynamixel','dxl_read_word',id(1),PRESENT_POSITION))
                    calllib('dynamixel','dxl_write_word',id(1),MOVING_SPEED,speed(1));

        calllib('dynamixel','dxl_write_word',id(1),GOAL_POSITION,round(position(1))-(slide_pos-pres_pos));
       display(position(1))
display(pres_pos)
display(slide_pos)
display(position(1)-(slide_pos-pres_pos))
        
        slide_pos=position(1);
        
pause(1)
        %calllib('dynamixel','dxl_write_word',id(1),GOAL_POSITION,position(1));
        %slide_pos=loc(j,2)-30;
        
        
        for i = [4,3,2];
            if position(i)>max_lim(i) || position(i)<min_lim(i)
                display(i)
                display(position(i))
                display('overlimit')
                
                pause
            else
                            calllib('dynamixel','dxl_write_word',id(i),MOVING_SPEED,speed(i));

                calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,position(i));
                pause(1)
            end
        end
        pause(4)
        %move move sliding motor to sweep (loc,2)+20
        goal_pos=(loc(j,2)+50)/slide_motor_rad*turn_scale*180/3.14;
        pres_pos=calllib('dynamixel','dxl_read_word',id(1),PRESENT_POSITION)
                    calllib('dynamixel','dxl_write_word',id(1),MOVING_SPEED,speed(1));

        calllib('dynamixel','dxl_write_word',id(1),GOAL_POSITION,round(goal_pos)-(slide_pos-pres_pos));
             display(goal_pos)
display(pres_pos)
display(slide_pos)
display(goal_pos-(slide_pos-pres_pos))
        
        slide_pos=goal_pos;
        pause(4)
        %calllib('dynamixel','dxl_write_word',id(1),GOAL_POSITION,(loc(j,2)+30)/slide_motor_rad*turn_scale*180/3.14);
        %slide_pos=loc(j,2)+30;
        
        %{
%move back to default position
 for i = 2:4
    calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,default1(i));
    pause(4)
 end
        %}
                    calllib('dynamixel','dxl_write_word',id(2),MOVING_SPEED,speed(2));

        calllib('dynamixel','dxl_write_word',id(2),GOAL_POSITION,position(2)+50);
        pause(2)
        
    else
    %%
    %grab domino
    display('Grabbing')
    [theta1,theta2]=invkin(loc(j,1)); %check 1 or 2
    position=[(loc(j,2))/slide_motor_rad*turn_scale*180/3.14,...
        (theta1-angle_to_zero_1)*angle_scale, ...
        (theta2-angle_to_zero_2)*angle_scale,claw_open]; %grabber open
    
    
    pres_pos=calllib('dynamixel','dxl_read_word',id(1),PRESENT_POSITION)
                calllib('dynamixel','dxl_write_word',id(1),MOVING_SPEED,speed(1));

    calllib('dynamixel','dxl_write_word',id(1),GOAL_POSITION,...
        round(position(1))-(slide_pos-pres_pos));
      display(position(1))
display(pres_pos)
display(slide_pos)
display(position(1)-(slide_pos-pres_pos))
    slide_pos=position(1);
    pause(4)
    %calllib('dynamixel','dxl_write_word',id(1),GOAL_POSITION,position(1));
    %slide_pos=loc(j,2);
    
    for i = [4,3,2]
        if position(i)>max_lim(i) || position(i)<min_lim(i)
            display('overlimit')
            display(i);
            pause
        else
                        calllib('dynamixel','dxl_write_word',id(i),MOVING_SPEED,speed(i));

            calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,position(i));
            pause(1)
        end
    end
    pause(3);
                calllib('dynamixel','dxl_write_word',id(4),MOVING_SPEED,speed(4));

    calllib('dynamixel','dxl_write_word',id(4),GOAL_POSITION,claw_close);%close grabber
    pause(2)
          calllib('dynamixel','dxl_write_word',id(2),MOVING_SPEED,speed(2));

    
    calllib('dynamixel','dxl_write_word',id(2),GOAL_POSITION,position(2)+50);
    pause(2)
    map_index = 0;
    
    
    display('MAP DOMINOS')
    %Decide where to put dominos
    for i=1:length(sortdots)
        if (domIDs(j,1)==sortdots(i,1) && domIDs(j,2)==sortdots(i,2)) || ...
                (domIDs(j,1)==sortdots(i,2) && domIDs(j,2)==sortdots(i,1))
            map_index=i;
            break
        end
    end
    
    % place domino in sorted position
    [theta1,theta2]=invkin(sortmap(map_index,2)); %check 1 or 2
    position=[((sortmap(map_index,1)))/slide_motor_rad*turn_scale*180/pi,...
        (theta1-angle_to_zero_1)*angle_scale, ...
        (theta2-angle_to_zero_2)*angle_scale,...
        claw_close]; %grabber open
    
    
    pres_pos=calllib('dynamixel','dxl_read_word',id(1),PRESENT_POSITION)
               calllib('dynamixel','dxl_write_word',id(1),MOVING_SPEED,speed(1));

    calllib('dynamixel','dxl_write_word',id(1),...
        GOAL_POSITION,round(position(1))-(slide_pos-pres_pos));
       display(position(1))
display(pres_pos)
display(slide_pos)
display(position(1)-(slide_pos-pres_pos))
    slide_pos=position(1);
    %calllib('dynamixel','dxl_write_word',id(1),GOAL_POSITION,position(1));
    %slide_pos=300+sortmap(i,2);
    pause(3)
%     last_pos = pres_pos;
%     pres_pos=calllib('dynamixel','dxl_read_word',id(1),PRESENT_POSITION)
%     dist = abs(pres_pos - last_pos)/4.2441;
%     disp(sprintf('dist: %d',dist));
    for i = [3,2]
        if position(i)>max_lim(i) || position(i)<min_lim(i)
            pause
            display('overlimit')
        else
               calllib('dynamixel','dxl_write_word',id(i),MOVING_SPEED,speed(i));

            calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,position(i));
            pause(2)
        end
    end
    %{
                   calllib('dynamixel','dxl_write_word',id(4),MOVING_SPEED,speed(4));

    calllib('dynamixel','dxl_write_word',id(4),GOAL_POSITION,position(4));
    pause(2)
    %}
                   calllib('dynamixel','dxl_write_word',id(4),MOVING_SPEED,speed(4));

    calllib('dynamixel','dxl_write_word',id(4),GOAL_POSITION,claw_open);%open grabber
    pause(2)
    %}
    
    end
end

calllib('dynamixel','dxl_terminate');
