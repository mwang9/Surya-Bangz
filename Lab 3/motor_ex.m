function [] = motor_ex(i,speed,steps)

id = 1:4;
GOAL_POSITION = 30; %target position
MOVING_SPEED = 32; %rotation speed
TORQUE_LIMIT = 34;
PRESENT_POSITION = 36; %current position
PRESENT_LOAD = 40;
MOVING = 46; %stationary = 0; moving = 1

position = int32(calllib('dynamixel','dxl_read_word',id(i),PRESENT_POSITION));
%fprintf('Before pos: %d \n',position);
calllib('dynamixel','dxl_write_word',id(i),MOVING_SPEED,speed);
%calllib('dynamixel','dxl_write_word',id(i),TORQUE_LIMIT,torque);
calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,steps);

move = calllib('dynamixel','dxl_read_byte',id(i),MOVING);
fprintf('Move: %d \n',move);

if move == false
    %disp('Did not move');
    calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,steps);
end

pos0 = 0;

while move
    move = calllib('dynamixel','dxl_read_byte',id(i),MOVING);
    pos1 = int32(calllib('dynamixel','dxl_read_word',id(i),PRESENT_POSITION));
    if pos1 == pos0
        %disp('STUCK');
        calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,steps);
    else
        pos0 = pos1;
    %fprintf('Current pos: %d \n',pos1);
    %disp(move);
    if move == false
        disp('Done moving');
        break
    end
end

end