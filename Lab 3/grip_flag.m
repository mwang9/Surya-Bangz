% determine if domino is gripped
setup

calllib('dynamixel','dxl_write_word',id(4),MOVING_SPEED,40);
calllib('dynamixel','dxl_write_word',id(4),GOAL_POSITION,600);
pause(3);

pos = (calllib('dynamixel','dxl_read_word',id(4),PRESENT_POSITION));
calllib('dynamixel','dxl_write_word',id(4),GOAL_POSITION,claw_fclose);

tic

%pause(1);

move = 1;
while move
    move = calllib('dynamixel','dxl_read_byte',id(4),MOVING);
    if move == false
        toc
    end
end

grip();

distance = abs(claw_close - PRESENT_POSITION)

%load = calllib('dynamixel','dxl_read_word',id(4),PRESENT_LOAD);
%fprintf('Load: %d \n',load);

calllib('dynamixel','dxl_write_word',id(4),GOAL_POSITION,claw_open);