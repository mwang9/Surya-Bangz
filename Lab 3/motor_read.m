PP = zeros(1,4);
CP = zeros(1,4);
M = [];

%read the position and movement status of each motor
for i = [1 2 3 4]
    pos = int32(calllib('dynamixel','dxl_read_word',id(i),PRESENT_POSITION));
    CP(i) = pos;
    move = calllib('dynamixel','dxl_read_byte',id(i),MOVING);
    M(i) = move;
end

%determine if any motors are NOT moving
[r ,] = find(move == 0);
flag = length(r); %if a motor isn't moving, flag will be true

%don't break until all motors are done moving
while flag < length(M)
    for i = [1 2 3 4]
        pos = int32(calllib('dynamixel','dxl_read_word',id(i),PRESENT_POSITION));
        CP(i) = pos;
        move = calllib('dynamixel','dxl_read_byte',id(i),MOVING);
        M(i) = move;
        if CP(i) == PP(i)
            %motor isn't moving
            %motor = (i,speed(i),));
        end    
    end
    PP = CP; %record the current position and delegate it as a past position
    [r ,] = find(move == 0);
    flag = length(r)
end

disp('Worked');