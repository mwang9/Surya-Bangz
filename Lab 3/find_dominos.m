function [barlocs,barOrientation,domIDs] = find_dominos(picture,flag)

size(picture)

%% Magic numbers
radius = 15;
<<<<<<< HEAD
checkrad = 8;
udotthresh = 0.01;  %uniqueness tolerance
ubarthresh = 0.02;  %uniqueness tolerance
Cdiffthresh = 20;         %max allowable difference in intensity between sides
whitethresh = 205;        %intensity over which a white part is valid
=======
checkrad = 6;
udotthresh = 0.01;  %uniqueness tolerance
ubarthresh = 0.02;  %uniqueness tolerance
Cdiffthresh = 20;         %max allowable difference in intensity between sides
whitethresh = 250;        %intensity over which a white part is valid
if flag
    disp('\n\n\n\n\n\n\n\n\n\n\n\n\nwhitethresh increased!!!!!!!!!!!!!!!!\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n')
    whitethresh = 240;
end
>>>>>>> rupert
dotintensitythresh = 195; %intensity under which a dot is considered valid
barintensitythresh = 180; %intensity under which a bar is considered valid
dotdiameter = 10;
doteccentricitythresh = 0.9;
bareccentricitythresh = 0.98;
barerrortolerance = 0.2;
barlength = 24;
RegionAreaRange = [10, 100];     %mser variables
MaxAreaVariation = 0.7;            %mser variables
ThresholdDelta = 1;              %mser variables
sixnzerosdil = 13;
edgethresh = 0.05;   %image processing
filter = 0.8;       %image processing
BWthresh = 0.9;     %image processing
%ID diagnosis constants
onesradthresh = 0.3; %max fraction of radius away from centre to be valid 1
%^^^ consider increasing if 3s are being predicted to much


%% Start Doing shit
tic
%retrieve and process webcam image
I= picture; %imread(picture);          %%%%% put name of image to be tested here
Ig=rgb2gray(I);

%mask sorting area
Ig=mask(Ig,120,130);
%         Ig = imadjust(Ig); %,[0.7 0.95],[0.1 0.9]);
Igf=imgaussfilt(Ig, filter); %filter shit
Igfe=edge(Igf,'Canny', edgethresh); %edge image
se = strel('square',sixnzerosdil);
imdil = imdilate(Igfe,se); 
level = graythresh(Igf*BWthresh);
BW = im2bw(Igf,level);

%search for MSER regions
[regions,mserCC] = detectMSERFeatures(Igf,'RegionAreaRange',RegionAreaRange...
    ,'MaxAreaVariation',MaxAreaVariation,'ThresholdDelta',ThresholdDelta);
% Measure the MSER region eccentricity to gauge region circularity.
stats = regionprops('table',mserCC,'Eccentricity','MajorAxisLength','Solidity');
% Threshold eccentricity values to keep circular and bar regions.
lenmidline = barlength;
higheccentricity = stats.Eccentricity > bareccentricitythresh;
midlinelength = abs(stats.MajorAxisLength - lenmidline) < barerrortolerance*lenmidline;
higheccentricity = higheccentricity & midlinelength;
loweccentricity = stats.Eccentricity < doteccentricitythresh;
smallarea = stats.MajorAxisLength < dotdiameter;
loweccentricity = loweccentricity & smallarea;
%defin bar and dot regions
barRegions = regions(higheccentricity);
dotRegions = regions(loweccentricity);
barlocs_ = barRegions.Location;
dotlocs_ = dotRegions.Location;

%filter bar & dot locations for overlapped regions so no double ups
[D, udotindexes, id] = uniquetol(dotlocs_,udotthresh,'ByRows',true);
[B, ubarindexes, ib] = uniquetol(barlocs_,ubarthresh,'ByRows',true);
% ubarlocs = uniquetol(barlocs_,ubarthresh,'ByRows',true);

%create logical selecting arrays to represent the filtering
% dotselect = ia;     %ismember(dotlocs_(:,1),udotlocs(:,1));
% barselect = ismember(barlocs_(:,1),ubarlocs(:,1));

%filter light dots out
unumdots = length(udotindexes);
udotfilter = [];
for dot = 1:unumdots
    dotindex = udotindexes(dot);
    dotpixels = dotRegions.PixelList(dotindex);
    numdotpixels = length(dotpixels(:,1));
    pixintensity = zeros(numdotpixels,1);
    for pixel = 1:numdotpixels
        pixintensity(pixel) = Igf(dotpixels(pixel,2),dotpixels(pixel,1));
    end
    dotintensity = mean(pixintensity);
    if dotintensity > dotintensitythresh
        %might be a six dot or inbetween region
        udotfilter = [udotfilter;dotindex];
    end
    %else is dark enough to be a proper domino dot
end

%filter light bars out
unumbars = length(ubarindexes);
ubarfilter = [];
for bar = 1:unumbars
    barindex = ubarindexes(bar);
    barpixels = barRegions.PixelList(barindex);
    numbarpixels = length(barpixels(:,1));
    pixintensity = zeros(numbarpixels,1);
    for pixel = 1:numbarpixels
        pixintensity(pixel) = Igf(barpixels(pixel,2),barpixels(pixel,1));
    end
    barintensity = mean(pixintensity);
    if barintensity > barintensitythresh
        %might be an invalid bar detection
        ubarfilter = [ubarfilter;barindex];
    end
    %else is dark enough to be a proper domino bar
end

%update selectors
dotselect = unique(setdiff(udotindexes,udotfilter));    %enact intensity filtering done by
barselect = unique(setdiff(ubarindexes,ubarfilter));    %     the two blocks above ^^^

%find number of bars/dots and properties like orientation and locs
barOrientation = barRegions.Orientation(barselect);
barlocs = barlocs_(barselect,:);
dotlocs = dotlocs_(dotselect,:);
numbars = length(barOrientation);
numdots = length(dotlocs(:,1));

%parse through bars and dots to identify dominos and false bars etc
LRCs = zeros(numbars,4);
LRcheckCs = zeros(numbars,3);
barlabels = cell(numbars,1);
IDlabels = cell(numbars,1);
domIDs = zeros(2,numbars);
%radius = 15;
for bar = 1:numbars
    Lnum = 0;
    Rnum = 0;

    %find centres of left and right domino sides
    LC = [(barlocs(bar,1)-radius*sin(barOrientation(bar))),...
            (barlocs(bar,2)-radius*cos(barOrientation(bar)))];
    RC = [(barlocs(bar,1)+radius*sin(barOrientation(bar))),...
            (barlocs(bar,2)+radius*cos(barOrientation(bar)))];

    %find check centres of left and right domino sides
    checkLC = [(barlocs(bar,1)-checkrad*sin(barOrientation(bar))),...
            (barlocs(bar,2)-checkrad*cos(barOrientation(bar)))];
    checkRC = [(barlocs(bar,1)+checkrad*sin(barOrientation(bar))),...
            (barlocs(bar,2)+checkrad*cos(barOrientation(bar)))];
        
    %check for any dots in the vicinity
    Lrads = [];
    Rrads = [];
    for k = 1:numdots
        dotloc = dotlocs(k,:);
        L = norm([abs(LC(1)-dotloc(1)),abs(LC(2)-dotloc(2))]);
        R = norm([abs(RC(1)-dotloc(1)),abs(RC(2)-dotloc(2))]);  
        if L < radius
            Lnum = Lnum+1;
            Lrads = [Lrads;L];
            %limit to 5 if six or more detected as it wont find a
            %shitty 6 and more is obviously wrong
            if Lnum > 5
                Lnum = 5;
            end;
        end
        if R < radius
            Rnum = Rnum+1;
            Rrads = [Rrads;R];
            %same thing
            if Rnum > 5
                Rnum = 5;
            end
        end
    end
    %%% Setup for troubleshooting domIDs
    for side = 1:2
        if side == 1
            %currently left side
            num = Lnum;
            C = LC;
            rads = Lrads;
        elseif side == 2;
            %currently right side
            num  = Rnum;
            C = RC;
            rads = Rrads;
        end
        
        %dot setup
        imC = round(C);
        imLC = round(LC);
        imRC = round(RC);
        
        %bar setup
        imcheckRC = round(checkRC);
        imcheckLC = round(checkLC);
        Lcheck = 0;
        Rcheck = 0;
        imcheckCdiff = 0;
        
        %check predicted domino side centre is still in image scope
        if max(fliplr(imLC)>size(BW)) || max(any(imLC<0))...
                || max(fliplr(imRC)>size(BW)) || max(any(imRC<=0))
            %if not in image, forget trying to ID it
            %false bar
            Lnum = -1;
            Rnum = -1;
            
        else
            %Check for invalid bars
            Lcheck = double(Igf(imcheckLC(2),imcheckLC(1)));
            Rcheck = double(Igf(imcheckRC(2),imcheckRC(1)));
            %update checkCs
            LRcheckCs(bar,:) = [Lcheck,Rcheck,abs(Lcheck-Rcheck)];%Rcheck+Lcheck];
            %Check side intensities and differences
            if Lcheck<whitethresh || Rcheck<whitethresh || abs(Rcheck-Lcheck)>Cdiffthresh
                %false bar
                Lnum = -2;
                Rnum = -2;
                break
            end
            
            %If full domino in image and bar valid,
            %check for IDing properties of dot area
            Centerbright = Igf(imC(2),imC(1));
            
            %% DIFFERENT NUMBER TROUBLESHOOTING HERE
            %Note that the troubleshooting assumes the likely missing of 
            %dot detection rather than over detection due to filters used
            
            %Initialize correction num variable
            newnum = -2;
            
            %Corrections for ZERO
            if num == 0
                if Centerbright>dotintensitythresh
                    %check for a shitty six
                    six = imdil(imC(2),imC(1));
                    if six
                        newnum = 6;
                    end
                    %if not a shitty six then a zero
                %if not a zero then not a valid bar
                else
                    %false bar
                    Lnum = -3;
                    Rnum = -3;
                    break
                end
            %...elseif carried on by validity checks for other numbers
            
            %Corrections for ONE
            elseif num == 1
                if rads(1) > onesradthresh*radius;
                    %most likely missed a dot and is a 2 or 3
                    if Centerbright>dotintensitythresh
                        %can't be a 3, so assume a 2
                        newnum = 2;
                    else
                        %most likely invalid though rarely may be a 3
                        %false bar
                        Lnum = -4;
                        Rnum = -4;
                        break
                    end
                end
            %...elseif carried on by validity checks for other numbers
            
            %Corrections for TWO
            elseif num == 2
                if rads(1:2) > onesradthresh*radius;
                    %most likely a correct 2
                    if Centerbright<dotintensitythresh
                        %can't be a 2, assume missed inner dot for a 3
                        newnum = 3
                    end
                else
                    %can't be a 2, assume missed outer dot for a 3
                    newnum = 3;
                end
            %...elseif carried on by validity checks for other numbers
            
            %Corrections for THREE
            elseif num == 3
                if rads(1:3) > onesradthresh*radius;
                    %most likely not a 3
                    %%weakly can't be a 3, assume missed outer dot for a 3
                    %newnum = 3
                    if Centerbright>dotintensitythresh
                    %strongly can't be a 3, missed outer dot for a 4
                        newnum = 4
                    end
                end
            %...elseif carried on by validity checks for other numbers
            
            %Corrections for FOUR
            elseif num == 4
                if all(rads(1:4) > onesradthresh*radius);
                    %most likely a correct 4
                    if Centerbright<dotintensitythresh
                        %can't be a 4, assume missed inner dot for a 5
                        newnum = 5
                    end
                else
                    %can't be a 4, assume missed inner dot for a 5
                    newnum = 5;
                end
            end
            
            %Check if any corrections triggered
            if newnum == -2
                newnum = num;
            end
            
            %Update corrected L/Rnums
            if side == 1
                Lnum = newnum;
            elseif side == 2;
                Rnum = newnum;
            end
            
        end
    end

%update alla shits
domIDs(:,bar) = [Lnum;Rnum];
TdomIDs = domIDs';
LRCs(bar,:) = [LC RC];
barlabels(bar) = {mat2str(round(barlocs(bar,:)))};
IDlabels(bar) = {mat2str(TdomIDs(bar,:))};
end
%update ID labels
barlabels = char(barlabels);
IDlabels = char(IDlabels);

%% Check for weird shit


%% Display IDs and stuff
fprintf('Dominoes found:\n');
disp(domIDs);
fprintf('Check centers:\n');
disp(LRcheckCs);
%         fprintf('\nDomino Orientations (degrees):\n');
%         disp(barOrientation*180/pi);
        fprintf('\nDomino Centroid Locations (degrees):\n');
        disp(barlocs);

%finding dominos finished, record time
toc

%% Show results
%         if i == 1
    figure
%         end
%         clf
imshow(Igf);
hold on
for bar = 1:numbars
%             text(double(barlocs(bar,1)),double(barlocs(bar,2)),...
%                 [barlabels(bar,:),'\rightarrow'],'Color','red','FontSize',14,...
%                 'HorizontalAlignment','right');
    text(double(barlocs(bar,1)),double(barlocs(bar,2)),...
        IDlabels(bar,:),'Color','red','FontWeight','bold','FontSize',8,...
        'HorizontalAlignment','center');
end
%% Troubleshooting figures:
% % Show all detected MSER Regions.
%{
figure
imshow(Igf)
hold on
plot(barRegions(barselect),'showPixelList',true,'showEllipses',true);

figure
imshow(Ig)
hold on
plot(regions,'showPixelList',true,'showEllipses',false,'showOrientation',false);
hold off

figure
imshow(BW);
hold on
viscircles(LRCs(:,1:2),radius*ones(numbars,1));
hold on 
viscircles(LRCs(:,3:4),radius*ones(numbars,1));
hold on 
plot(dotlocs(:,1),dotlocs(:,2),'r*');

figure
imshow(Igfe);
hold on
plot(dotRegions(dotselect),'showPixelList',true,'showEllipses',false);
hold on
plot(barlocs(:,1),barlocs(:,2),'g*');

figure
imshow(imdil);

figure
imshow(BW);
% %         hold on;
% %     
% %         viscircles(centers, radii,'EdgeColor','b');
%}
end