function I = test_camera()

tic;

cam = webcam(1);
cam.Resolution = '640x360';
I = snapshot(cam);
size(I)
clear cam

toc;

fprintf('\nphoto taken\n');
end