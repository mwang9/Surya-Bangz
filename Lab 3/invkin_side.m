function [Theta1,Theta2] = invkin_side(loc);
%set x and y locations
px = loc(1);
py = loc(2);

L1=205; %230;
L2=310; %285;
px=620-153-px; %swap axis and add offsets
py=335-py+0;       %add offsets
%L1 = 190;
%L2 = 170;
% h = 125;
%h=145


%px = x;
%py = h;
%px = 300
% py = -h;

k = (px^2 +py^2 - L1^2 - L2^2)/(2*L1*L2);
theta2 = acosd(k);
theta2n = -theta2;

delta = L1^2 + L2^2 + 2*L1*L2*cosd(theta2);
ct1 = (px*(L1 + L2*cosd(theta2))+ py*L2*sind(theta2))/delta;
st1 = (-px*L2*sind(theta2)+ py*(L1+L2*cosd(theta2)))/delta;

deltan = L1^2 + L2^2 + 2*L1*L2*cosd(theta2n);
ct1n = (px*(L1 + L2*cosd(theta2n))+ py*L2*sind(theta2n))/delta;
st1n = (-px*L2*sind(theta2n)+ py*(L1+L2*cosd(theta2n)))/delta;


theta1 = round(atan2d(st1, ct1));
theta1n = round(atan2d(st1n, ct1n));
theta2 = round(theta2);
theta2n = round(theta2n);

if abs(theta1n ) <80
    Theta1=theta1n
    Theta2=theta2n
else 
    Theta1=theta1n
    Theta2=theta2n
end
