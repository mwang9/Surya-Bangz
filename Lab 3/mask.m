function maskedImage = mask(dompic,ycutoff,xcutoff);
    [y, x] = size(dompic);
    unmasked = ones((y-ycutoff),(x-xcutoff));
    ymask = zeros(ycutoff,(x-xcutoff));
    xmask = zeros(y,xcutoff);
    fullMask = uint8(vertcat(ymask,unmasked));
    fullMask = uint8(horzcat(fullMask,xmask));
    maskedImage = dompic.* fullMask;
end