%% import library and functions
loadlibrary('dynamixel','dynamixel.h');
libfunctions('dynamixel');

%define port & baud
DEFAULT_PORTNUM = 8;  %COM8
DEFAULT_BAUDNUM = 1;  %1Mbps

%open device
res = calllib('dynamixel','dxl_initialize',DEFAULT_PORTNUM,DEFAULT_BAUDNUM);

%assign control variables
id = 1:4;
GOAL_POSITION = 30;
MOVING_SPEED = 32;
TORQUE_LIMIT = 34;
PRESENT_POSITION = 36;
PRESENT_SPEED = 38;
PRESENT_LOAD = 40;
MOVING = 46;

%positions
default = [2048,512,512,500];
pos = [3096,200,750,450;
    1099,300,600,600;
    4096,512,300,300];

%other settings
speed = [40,45,45,30];
torque = [750,750,500,100];

count = 0;

for i = 1:4
    [count] = motor(i,speed(i),torque(i),default(i),count);
end

for j = 1:size(pos,1)
    for i = 1:4
        [count] = motor(i,speed(i),torque(i),pos(j,i),count);
    end
end

fprintf('Count: %d \n',count);
% calllib('dynamixel','dxl_write_word',id(i),MOVING_SPEED,20); %speed of 40
% p_pos = int32(calllib('dynamixel','dxl_read_word',id(i),PRESENT_POSITION)) %current pos
% diam = 29; %variable
% ratio = 4000/(pi*diam); %steps per mm (distance -> steps
% dist = 300; %distance
% calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,steps);%p_pos+(dist*ratio)); %just adds dist
% mov = calllib('dynamixel','dxl_read_byte',id(i),MOVING);
% fprintf('Moving: %d \n',mov);
% while moving
% pause(5)
% mov = calllib('dynamixel','dxl_read_byte',id(i),MOVING);
% if mov == 0
%     calllib('dynamixel','dxl_write_word',id(i),GOAL_POSITION,steps2);
% end
% end
% calllib('dynamixel','dxl_write_word',id(1),GOAL_POSITION,2000);
calllib('dynamixel','dxl_terminate');