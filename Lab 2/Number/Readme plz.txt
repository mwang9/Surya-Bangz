To test out domino identification code:
execute the following:

>>egImagelistmaker
>>isFullset(Imlist)



File descriptions:

IDdom:  Function
Takes in the name of an image file (tif,png,jpg.. doesn't matter) which must be in the same path
Returns a 2x1 array of the values on the domino

egImagelistmaker:   Script
Makes a cell array (called Imlist) of the image file names (skew1_d#.tif) included in this zip file, which were used for testing
of isFullset.

isFullset:   Function
Takes in a cell array of image file names (an example is created by egImagelistmaker)
Returns a string notifying whether all dominoes in the recieved image files make up a full set or whether
there are duplicates/missing dominoes and how many. The list of detected dominoes is also included.
This returned string can be concatenated with other strings or just printed to the output window by whatever script
is calling isFullset
