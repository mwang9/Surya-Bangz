function DominoID = IDdom(dom_face_pic)
%% Description
% Takes in the name of an image file (tif,png,jpg.. doesn't matter) which
% must be in the same path. Returns a 2x1 array of the values on the domino

%% Input
I=imread(dom_face_pic);
Ig=rgb2gray(I);
%Igf=imgaussfilt(Ig, 1); %filter shit resolution
Igfe=edge(Ig,'Canny'); %edge image

% level = graythresh(pic);
% BW = im2bw(pic,level);

corners = detectHarrisFeatures(Igfe);


%% Find lines from Hough Transform peaks
[H,T,R] = hough(Igfe,'RhoResolution',0.4,'ThetaResolution',0.4);

P  = houghpeaks(H,10,'threshold',ceil(0.6*max(H(:))));

lines = houghlines(Igfe,T,R,P,'FillGap',30,'MinLength',12);
%lines(13)
rm = [];
linesx = [];
tthresh = 3;
for k = 1:length(lines)
    linetheta = lines(k).theta;
    linex = lines(k).point1(1);
    if linetheta<-tthresh || linetheta>tthresh || any(linesx == linex)
        rm = [rm k];
    end
    linesx = [linesx linex];
end
if ~isempty(rm)
   lines(rm) = [];
end
numlines = length(lines);
    
%% Find midpoint line
if ~numlines
    midpoint = length(Igfe)/2;
else
    midptsum = 0;
    for i = 1:numlines
        midptsum = midptsum + lines(i).point1(1);
    end
    midpoint = midptsum/numlines;
end

%% Find circles from Hough Transform
%Turn off warning for use of houghcircles with radii<10 (still works fine)
id = 'images:imfindcircles:warnForSmallRadius';
warning('off',id);

[centers, radii] = imfindcircles(Igfe,[3 5],'Sensitivity',0.98);

%Turn all warnings back on
warning('on','all');

%Filter out possible false circles detected from midline
del = [];
lrthresh = 4;
for k = 1:length(centers)
    cen = centers(k,1);
    if (midpoint-lrthresh)<=cen && cen<=(midpoint+lrthresh)  %%Set for middle of the picture at midline
        del = [del k];
    end
end
if ~isempty(del);
    centers(del,:) = [];
    radii(del) = [];
end
%centers
%radii

%diagnose num dots on left and right sides
leftnum = 0;
rightnum = 0;
for j = 1:length(centers)
    cen = centers(j,1);
    if cen < midpoint
        leftnum = leftnum + 1;
    else
        rightnum = rightnum + 1;
    end
end

%% Checks
if leftnum+rightnum == length(centers)
    %fprintf('All dots accounted for\n');
else
    fprintf('Dots missing/not accounted for!!\n');
end
if leftnum > 6 || rightnum > 6
    fprintf('Identification failed - combination nonexistent!!\n')
end

%% Identify Domino
%fprintf('Domino: %d|%d\n',leftnum,rightnum);
DominoID = [leftnum; rightnum];
    
%% Plot data and images
% subplot(2,3,2);
% imshow(Igfe);
% title('Canny Edges.png');
% 
% subplot(2,3,3);
% imshow(I);
% viscircles(centers, radii,'EdgeColor','r');
% title('Dots.png');
% 
% subplot(2,3,1);
% imshow(I);
% hold on;
% plot(corners);
% title('Image & corners.png');
% 
% subplot(2,3,4);
% imshow(imadjust(mat2gray(H)),'XData',T,'YData',R,...
%       'InitialMagnification','fit');
% hold on;
% x = T(P(:,2)); y = R(P(:,1));
% plot(x,y,'s','color','white');
% title('Hough transform of Image.png');
% xlabel('\theta'), ylabel('\rho');
% axis on, axis normal, hold on;
% colormap(gca,hot);
% 
% subplot(2,3,5);
% imshow(I), hold on;
% max_len = 0;
% for k = 1:length(lines)
%    xy = [lines(k).point1; lines(k).point2];
%    plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');
% 
%    Plot beginnings and ends of lines
%    plot(xy(1,1),xy(1,2),'x','LineWidth',2,'Color','yellow');
%    plot(xy(2,1),xy(2,2),'x','LineWidth',2,'Color','red');
% 
%    Determine the endpoints of the longest line segment
%    len = norm(lines(k).point1 - lines(k).point2);
%    if ( len > max_len)
%       max_len = len;
%       xy_long = xy;
%    end
% end
% title('Midlines.png');

end