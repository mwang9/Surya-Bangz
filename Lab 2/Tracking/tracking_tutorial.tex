\documentclass{article}
\usepackage[a4paper, margin=1in]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{textcomp}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{blindtext}
\usepackage{enumitem}
\usepackage{bm}
\usepackage{courier}
\usepackage{amssymb}
\usepackage{mathtools}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=true,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,	                   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Matlab,                 % the language of the code
  otherkeywords={*,...},           % if you want to add more keywords to the set
  numbers=none,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=4,	                   % sets default tabsize to 2 spaces
  title=\lstname,                   % show the filename of files included with \lstinputlisting; also try caption instead of title
  upquote=true,
}
\renewcommand{\lstlistingname}{Script}

\hypersetup{
    bookmarks=true,         % show bookmarks bar?
    unicode=false,          % non-Latin characters in Acrobat’s bookmarks
    pdftoolbar=true,        % show Acrobat’s toolbar?
    pdfmenubar=true,        % show Acrobat’s menu?
    pdffitwindow=false,     % window fit to page when opened
    pdfstartview={FitH},    % fits the width of the page to the window
    pdftitle={My title},    % title
    pdfauthor={Author},     % author
    pdfsubject={Subject},   % subject of the document
    pdfcreator={Creator},   % creator of the document
    pdfproducer={Producer}, % producer of the document
    pdfkeywords={keyword1, key2, key3}, % list of keywords
    pdfnewwindow=true,      % links in new PDF window
    colorlinks=true,       % false: boxed links; true: colored links
    linkcolor=red,          % color of internal links (change box color with linkbordercolor)
    citecolor=green,        % color of links to bibliography
    filecolor=magenta,      % color of file links
    urlcolor=cyan           % color of external links
}

\newcommand\tab[1][1cm]{\hspace*{#1}}

\newcommand{\icol}[1]{% inline column vector
  \left[\begin{smallmatrix}#1\end{smallmatrix}\right]%
}

\newcommand{\irow}[1]{% inline row vector
  \begin{smallmatrix}[#1]\end{smallmatrix}%
}

\newcommand\numberthis{\addtocounter{equation}{1}\tag{\theequation}}

\begin{document}
\title{2D Template Tracking with Iterative Least Squares}
\date{September 2016}
\author{Patrick Mahoney}

\maketitle

\section{Reading}

Read sections 6.1 (2D and 3D feature-based alignment) and 6.2 (Pose Estimation) from Szeliski for this tutorial. Section 6.2 is a good explanation of the extrinsic pose recovery (3D Location / Orientations). Section 6.3 covers intrinsic camera calibration.

\section{Problem}

It is helpful to have a method to track the movement of an element from frame 
to frame. One approach for template tracking, to rely on an image of the 
element being tracked which is then searched for in the new frame. Let us 
define $I_0$ as the template image (eg. $100\times100$ pixels) and $I_f$ as an 
image frame in a sequence (eg. $1280\times720$ pixels)

In order to determine the location of the template in the new frame we seek to
evaluate how closely the template matches a specific location in the frame.
For this we will use the sum of squared differences between the template and
the image. This provides a measure for how good a match is.

\section{Tracking Translation}

To match the template to the image we need a transform $\bm{H}$ that allows us to map a point $\bm{w} = \icol{u\\v}$ in the template to a point $\bm{w'}$ in the frame. This is done by some function $\bm{w'} = f(\bm{w}, \bm{H})$.

If we consider that the element is moving in the frame with translation only then we can simply define $\bm{H} = \bm{t} = \icol{x\\y} : (x, y) \in \mathbb{R}$ \footnote{Note: We allow $\bm{t}$ to be a Real ($\mathbb{R}$) value and not an Integer ($\mathbb{Z}$). This will allow for `sub-pixel' motion estimation as well as expansion to more general transforms.}. In this case, we can state the mapping $\bm{w} \to \bm{w'}$ as $\bm{w'} = \bm{w} + \bm{H} = \bm{w} + \bm{t}$.

As $\bm{t} \in \mathbb{R}$ we may need to interpolate in 2D between points when evaluating a mapping between the original template and a subsequent view. For the purpose of this guide we will be using bilinear interpolation. This gives us, $\bm{I}_n(\bm{w}) = \text{interp2}(\bm{I}_n[\bm{w}])$.

For implementation, we vectorise both $\bm{I_f}$ and $\bm{I_0}$ as:

\begin{align*}
\bm{\alpha}[n \cdot m, 1] &= \text{vec}(\bm{I}) = \text{vec}\begin{pmatrix}
  \begin{bmatrix}
  a_{11} & a_{12} & \cdots  & a_{1n} \\
  a_{21} & a_{22} & \cdots  & a_{2n} \\ 
  \cdots & \cdots & \ddots  & \vdots \\
  a_{m1} & a_{m2} & \cdots  & a_{mn} \\
  \end{bmatrix}
\end{pmatrix}
=
\begin{bmatrix}
a_{11}\\a_{12}\\ \vdots \\a_{1n}\\ \vdots \\ a_{mn}
\end{bmatrix} \numberthis \label{eq:chi_func}
\end{align*}

Therefore, 

\begin{align*}
\bm{\alpha_f}[\bm{w}] &= \text{vec}(\text{interp}(\bm{I_f}[\bm{w}])) \numberthis \label{eq:alpha_f}\\
\bm{\alpha_0}[\bm{w}] &= \text{vec}(\bm{I_0}[\bm{w}]) \numberthis \label{eq:alpha_0}
\end{align*}

From this we can evaluate the vector of differences as $\bm{r}[n] = \bm{\alpha_f}[\bm{w'}] - \bm{\alpha_0}[\bm{w}]$.

Secondly we also want to know the derivative of the residual wrt. each of the variables. Analytically this can be evaluated as seen in Equation \ref{eq:residual_pde}.

\begin{align*}
  \frac{\partial \bm{r}}{\partial t_x} &= \frac{\partial}{\partial t_x} (\bm{\alpha_f}(\bm{w'}) - \bm{\alpha_0}(\bm{w})) \numberthis \label{eq:residual_pde}\\
  &= \frac{\partial \bm{\alpha_f}}{\partial \bm{w_{f}}} \cdot \frac{\partial \bm{w_{f}}}{\partial t_x}\\
  &= \nabla_x \bm{\alpha_f}(\bm{w'}) \cdot 1 
\end{align*}

We then want to scale the residual based on the expected change due to each variable giving us Equation \ref{eq:residual_devision}. As per Szeliski Section 6.1.3 we use an iterative algorithm to determine the translation.

\begin{align*}
q_x^k[n] &= \frac{r_n}{\frac{\partial \bm{r}}{\partial t_x}},q_y^k[n] = \frac{r_n}{\frac{\partial \bm{r}}{\partial t_y}} \numberthis \label{eq:residual_devision}
\end{align*}

We can now outline the update equation, shown in Equation \ref{eq:update_equation}.

\begin{align*}
\begin{bmatrix}
1 & 0\\
1 & 0\\
\vdots & \vdots\\
0 & 1\\
0 & 1\\
\vdots & \vdots\\
\end{bmatrix}
\begin{bmatrix}
t_x^{k+1} \\
t_y^{k+1} \\
\end{bmatrix}
&=
\begin{bmatrix}
t_x^k - q_x[0]\\
t_x^k - q_x[1]\\
\vdots\\
t_y^k - q_y[0]\\
t_y^k - q_y[1]\\
\vdots\\
\end{bmatrix} \numberthis \label{eq:update_equation}
\end{align*}
As this fits the form of $\bm{A}\bm{x} = \bm{b}$ this can be solved with a linear least squares approach. In Matlab consider using `mldivide' or `\\' to solve for $\bm{x}$. By comparing the update difference $\bm{t}^{k+1} - \bm{t}^{k}$ it is possible to determine when updates have reached a stable solution as the difference tends towards zero.

\section{Example solution}
\subsection{Use}
The example code provided should allow you to test the performance of the provided tracked. It uses the provided dataset of linear domino movement however this can quickly be swapped out for existing data or a camera feed.

Running the Matlab script `track\_main.m' will allow you to test the tracker. It loads in the data set to a cell array and prompts the user to select the region to track from the first frame. The code then progresses to track the region through consecutive images.

Some things of note:

\begin{itemize}
\item The tracking function takes images in grayscale double format. This means that images that are being tracked on must be converted first. 
\item When using the provided dataset it is of note that tracking is lost after the domino has progressed across about half the image. Consider why this is and possible solutions to this behaviour.
\item Currently the tracking location is signified by a point plotted at the top left corner of the tracked region. A better method of this is using the returned solution to draw a box or polygon around the tracked region.
\end{itemize}

\section{Extensions}

\subsection{Illumination Compensation}

As can be seen with the demo set, the current solution does not compensate for illumination variation, globally or locally. How does this affect the tracking ability and why we need brightness constancy here. Consider what changes are required required to solve for this, and implement these changes.

\subsection{Tracking Transforms}

The solution only only considers changes in translation for the element tracked. To robustly track an element it is necessary to solve for at least a similarity transform across the element. Consider the changes required for this correction. Szeliski Table 6.1 may help as a reference for this.

\subsection{Performance Improvements}

There are a number of changes that can be made to improve performance in both the solution and the supplied example code. What changes can be made to increase the efficiency of tracking and overhead required for performing the tracking operation. 

Take a look at the variables defined each time the tracking function is run. Are there any variables that are constant or do not need to be defined new each round. Change the scope of this definition.

It is also useful to consider the scale at which the tracking is performed. Is the full resolution as used in the demo required. It may be useful to consider Matlabs `impyramid' for this.

\clearpage
\section{Code}
\subsection{Solver}
\lstinputlisting[caption={track\_template.m}]{track_template.m}
\clearpage
\subsection{Main}
\lstinputlisting[caption={track\_main.m}]{track_main.m}

\end{document}