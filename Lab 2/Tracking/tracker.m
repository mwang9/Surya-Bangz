vid = VideoReader('domvid1.wmv');

%set(vid, 'FramesPerTrigger', 50);
%set(getselectedsource(vid), 'FrameRate', '10.0000');

%start(vid)

%wait(vid)
[f, t] = getdata(vid);

imtool(f(:,:,:,10))

vid

delete(vid)
clear vid

numframes = size(f, 4);
for k = numframes:-1:1
g(:, :, k) = rgb2gray(f(:, :, :, k));
end

for k = numframes-1:-1:1
d(:, :, k) = imabsdiff(g(:, :, k), g(:, :, k+1));
end
imtool(d(:, :, 1), [])

thresh = graythresh(d)
bw = (d >= thresh * 255); imtool(bw(:, :, 1))

bw2 = bwareaopen(bw, 20, 8);

s = regionprops(bwlabel(bw2(:,:,1)), 'centroid');
c = [s.Centroid]
%c = [226.8231 53.1538 260.3750 43.3167] 

background = imdilate(g, ones(1, 1, 5));
imtool(background(:,:,1))

d = imabsdiff(g, background);
thresh = graythresh(d);
bw = (d >= thresh * 255);

centroids = zeros(numframes, 2);
for k = 1:numframes
L = bwlabel(bw(:, :, k));
s = regionprops(L, 'area', 'centroid');
area_vector = [s.Area];
[tmp, idx] = max(area_vector);
centroids(k, :) = s(idx(1)).Centroid;
end




