%% Load In Images
%run test_camera
imagefiles = dir('data/*.png'); %grab images in same folder *.png     
nfiles = length(imagefiles);    % Number of files found

images = cell(1, nfiles); %set each image into array
for i=1:nfiles
   currentfilename = fullfile('data', imagefiles(i).name);
   currentimage = imread(currentfilename);
   images{i} = im2double(currentimage);
end

%replace with Rupert's images at set frame
%set time count using Rupert's frame rate

%% Get and display 200 frames from vi devices
figure('Position', [100, 100, 800, 500]); %create plot window

% There are many ways to plot an image
% 'imshow' tends to be the easiest how ever it is slow
% the following constructs an image object that can
% have its data overwritten directly improving image display
% performance

% Setup plot
set(gca,'units','pixels'); %
% Aquire size of video image format
sz = size(images{1});

% Construct image display object
cim = image(...
    [1 sz(2)],...
    [1 sz(1)],...
    zeros(sz(1), sz(2), 1),...
    'CDataMapping', 'scaled'...
);

% Ensure axis is set to improve display
colormap gray;
axis image;

%% Setup tracking

im = imgaussfilt(images{1}, 4);

% Update data in image display objects
set(cim, 'cdata', images{1});

drawnow;

template_rect = round(getrect); %select a rectangle

template = im(...
    template_rect(2):(template_rect(2) + template_rect(4)), ...
    template_rect(1):(template_rect(1) + template_rect(3)) ...
    );

%Initialise Result from first frame
[result, it, res] = track_template(im, template, [template_rect(1); template_rect(2)]);
fprintf('Initialised track location to X: %g, Y: %g, it: %i, r: %g\n', result(1), result(2), it, res);


%% Begin tracking
result_handle = 0;
for i = 2:nfiles

    % Get the frame and metadata.
    im = imgaussfilt(images{i}, 4);

    if result_handle ~= 0
        delete(result_handle);
    end

    % Update data in image display objects
    set(cim, 'cdata', images{i});

    [new_result, it, res] = track_template(im, template, result);

    % Output tracking results
    if isnan(res)
        fprintf('Lost tracking... initialising with previous result');
    else
        fprintf('Tracked template to X: %g, Y: %g, it: %i, r: %g\n', result(1), result(2), it, res);
    end

    % Update if result is ok
    if ~any(isnan(new_result))
        result=new_result;
    end

    % Draw result position on image
    hold on;
    result_handle = scatter(result(1), result(2));
    hold off;

    % Force a draw update
    drawnow;
end
