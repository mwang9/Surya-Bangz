vid = VideoReader('domvid6.wmv');
info = get(vid);
%D = info.NumberOfFrames;
%fr = info.FrameRate;
%Frames = fix(D/fr);
DominoOne = [];
DominoTwo = [];
DominoThree = [];
DominoFour = [];
video = vid.read();

%threshhold value
int = 0.3;

%%%%%%%BLOB%%%%%%%%%%%%%
%set up blob
blobAnalysis = vision.BlobAnalysis(...
    'AreaOutputPort', true,...
    'CentroidOutputPort', true,...
    'BoundingBoxOutputPort', true,...
    'MinimumBlobArea', 200,...
    'ExcludeBorderBlobs', true);

currentCentroid = [0 0];
moveFrame = 10;

for i = 30:5:90
    boxes = [];
    k = i
    pic1 = video(:,:,:,(i-moveFrame));
    pic2 = video(:,:,:,i);
    move = pic2-pic1;
    BW = im2bw(move, int);
    [areas, centroid, boxes] = step(blobAnalysis, BW);
    boxCheck = size(boxes);
    previousCentroid = currentCentroid;
    if boxCheck(1)==0
        %nothing has moved or something crazy happened
        d ='hi'
    else
        index = 1;
        %check what moved
        [~, idx] = sort(areas, 'Descend');
        boxes = double(boxes(idx(1:1), :));
        scale = 1;
        DetectedDomino = imresize(BW, scale);
        siz = size(centroid);
        for i=1:siz
            if  (centroid(i)> boxes(1))&&(centroid(i)< (boxes(1)+boxes(3)))
                %then it might be centroid
                if (centroid(i,2)> boxes(3))&&(centroid(i,2)< (boxes(2)+boxes(4)))
                    %then it is the centroid woo
                    currentCentroid = [centroid(i) centroid(i,2)]
                    pixelDist = norm(currentCentroid - previousCentroid) %pixels
                    pixelSpeed = (pixelDist/moveFrame)*info.FrameRate
                end
            end
        end
        DetectedDomino = insertObjectAnnotation(double(DetectedDomino), 'rectangle', ...
            scale * boxes, 'domino');
        figure; 
        imshow(DetectedDomino);
        hold on
        plot([single(currentCentroid(1))], [single(currentCentroid(1,2))], 'r.');
        hold off
        title('track domino');
        index = index +1;
    end
    pause(1)
end


%superimpose images
% C = imfuse(A,B)


