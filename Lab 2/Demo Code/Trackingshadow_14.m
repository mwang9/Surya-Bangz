% %%Camera Calibration
clear all;
clc;

test_camera(1);

% foto = 'Dominoes (1).jpg';
% file = imread(foto);

A = imread('domino_1.png');

% A = imread('Dominoes (3).png');
figure; imshow(A);
title('Input Image');

%% Filter out yellow colours
I=A;
range =[45,20];

    % RGB to HSV conversion
     I = rgb2hsv(I);
    
    % Normalization range between 0 and 1
    range = range./360;
    
    % Mask creation
    if(size(range,1) > 1), error('Error. Range matrix has too many rows.'); end
    if(size(range,2) > 2), error('Error. Range matrix has too many columns.'); end

    if(range(1) > range(2));
        % Red hue case
        mask = (I(:,:,1)>range(1) & (I(:,:,1)<=1)) + (I(:,:,1)<range(2) & (I(:,:,1)>=0));
    else
        % Regular case
        mask = (I(:,:,1)>range(1)) & (I(:,:,1)<range(2));
    end
    
    % Saturation is modified according to the mask
    I(:,:,2) = mask .* I(:,:,2);
    
    % HSV to RGB conversion
     I = hsv2rgb(I);
    imshow(I,[]);
%%  colorfilter
range =[6,60];


    % RGB to HSV conversion
    % RGB -->   Red, Blue, Green Layers above each other
    %
    % HSV -->   Hue         = color / tint  
    %           Saturation  = shade / amount of grey in it
    %           Value       = brightness / luminance
    % Red       = [0,60]
    % yellow    = [60,120]
    % green     = [120,180]
    % cyan      = [180,240]
    % blue      = [240,300]
    % magenta   = [300,360]
    
    I = rgb2hsv(I);
    
    % Normalization range between 0 and 1
    range = range./360;
    
    % Mask creation
    if(size(range,1) > 1), error('Error. Range matrix has too many rows.'); end
    if(size(range,2) > 2), error('Error. Range matrix has too many columns.'); end

    if(range(1) > range(2));
        % Red hue case
        mask = (I(:,:,1)>range(1) & (I(:,:,1)<=1)) + (I(:,:,1)<range(2) & (I(:,:,1)>=0));
    else
        % Regular case
        mask = (I(:,:,1)>range(1)) & (I(:,:,1)<range(2));
    end
    
    % Saturation is modified according to the mask
    I(:,:,3) = mask .* I(:,:,3);
    
% HSV to RGB conversion
I = hsv2rgb(I);
figure; imshow(I);
title('Colorfiltered Image');
    
%% Segment/track the shadows

% Convert the image to the HSV color space.
imHSV = rgb2hsv(I);

% Get the saturation channel.
saturation = imHSV(:, :, 3);

% Threshold the image
t = graythresh(saturation);
imDomino = (saturation > t);

figure; imshow(imDomino)
title('Segmented shadows');

% imDomino = imclearborder(imDomino);
% imDomino = bwmorph(imDomino, 'bridge');

se = strel('pair',[1,1]);        
imDomino = imdilate(imDomino,se);
se = strel('pair',[2,2]);   
imDomino = imdilate(imDomino,se); 
se = strel('pair',[1,1]);        
imDomino = imdilate(imDomino,se);
se = strel('pair',[2,2]);   
imDomino = imdilate(imDomino,se);

%% �

c = imDomino;

%// Get the region properties and select that with the largest area.
S = regionprops(c,'BoundingBox','FilledArea','Centroid','b','PixelList','Orientation','PixelIdxList');

figure; imshow(c);

boundingboxes = cat(1, S.BoundingBox);
FilledAreas = cat(1, S.FilledArea);
pixellist = cat(1,S.PixelList);
PixelIdxList = cat(1,S.PixelIdxList);
Orientation = cat(1,S.Orientation);
Center = cat(1,S.Centroid);

% sorting FilledAreas by highest value first, and saving the original ...
% position in array in O
[FilledAreas,O] = sort(FilledAreas, 'Descend');

figure; imshow(A);
title('box-surrounded shadows')

% amountboxes = max(size(boundingboxes(:,1)));
amountboxes = 20;
i=1;
for i = i : 1 : amountboxes;
    
    rectangle('Position', boundingboxes(O(i,1),:), 'Curvature', [0.5 1], 'EdgeColor','b');
    i = i+1;
end
