% %%Camera Calibration

InputImage = imread('Domino.png');
A=InputImage;

imagefiles = dir('Test/*png');      
numImages = length(imagefiles);    % Number of files found

numImages=1;
files = cell(1, numImages);

for i = 1:numImages
    files{i} = fullfile(matlabroot, 'toolbox', 'vision', 'visiondata', ...
        'calibration', 'slr', sprintf('image%d.png', i));
end

imOrig = imread(fullfile(matlabroot, 'toolbox', 'vision', 'visiondata', ...
        'calibration', 'slr', 'image9.jpg'));

   magnification = 100;   
    figure; imshow(imOrig, 'InitialMagnification', magnification);
title('Input Image');

 
  
%% Read the image Of objects to be measured


%% Filter out red colours

imshow(A);
A= im2double(A);
range =[360,200];

    % RGB to HSV conversion
     I = rgb2hsv(A);
    
    % Normalization range between 0 and 1
    range = range./360;
    
    % Mask creation
    if(size(range,1) > 1), error('Error. Range matrix has too many rows.'); end
    if(size(range,2) > 2), error('Error. Range matrix has too many columns.'); end

    if(range(1) > range(2));
        % Red hue case
        mask = (I(:,:,1)>range(1) & (I(:,:,1)<=1)) + (I(:,:,1)<range(2) & (I(:,:,1)>=0));
    else
        % Regular case
        mask = (I(:,:,1)>range(1)) & (I(:,:,1)<range(2));
    end
    
    % Saturation is modified according to the mask
    I(:,:,2) = mask .* I(:,:,2);
    
    % HSV to RGB conversion
    I = hsv2rgb(I);
    imshow(I,[]);

%% Filter out yellow colours

range =[45,20];

    % RGB to HSV conversion
     I = rgb2hsv(I);
    
    % Normalization range between 0 and 1
    range = range./360;
    
    % Mask creation
    if(size(range,1) > 1), error('Error. Range matrix has too many rows.'); end
    if(size(range,2) > 2), error('Error. Range matrix has too many columns.'); end

    if(range(1) > range(2));
        % Red hue case
        mask = (I(:,:,1)>range(1) & (I(:,:,1)<=1)) + (I(:,:,1)<range(2) & (I(:,:,1)>=0));
    else
        % Regular case
        mask = (I(:,:,1)>range(1)) & (I(:,:,1)<range(2));
    end
    
    % Saturation is modified according to the mask
    I(:,:,2) = mask .* I(:,:,2);
    
    % HSV to RGB conversion
%     I = hsv2rgb(I);
    imshow(I,[]);
    
%% Undistort the image

[im, newOrigin] = undistortImage(I, cameraParameters, 'OutputView', 'full');
 figure; imshow(im);
 title('Undistorted Image');

%% Segment/track the shadows

% Convert the image to the HSV color space.
imHSV = rgb2hsv(im);

% Get the saturation channel.
saturation = imHSV(:, :, 1);

% Threshold the image
t = graythresh(saturation);
imDomino = (saturation > t);

figure; imshow(imDomino);
title('Segmented shadows');

%% 
BW = imDomino;
imshow(BW,[]);


% %I is your image
% M = repmat(all(~BW,3),[1 1 3]); %mask black parts
% BW(M) = 255; %turn them white

imshow(BW,[]);

% Image_r = BW(:,:,1);
% % Suppose the value of "brown pixel" that you want to replace is 120.
% 
% ind_plain = find(Image_r  == 255);
% % "ind_plain" contains the indices of all pixels with those values
% 
% Image_r (ind_plain) = 255
% 
% imshow(Image_r,[]);

se = strel('line',5,60);        
dilateddBW = imdilate(BW,se);

k = ones(1081,1921);
c = xor(k,dilateddBW);

%// Get the region properties and select that with the largest area.
S = regionprops(c,'BoundingBox','FilledArea','b','PixelList','Orientation','PixelIdxList');

imshow(c,[]);
boundingboxes = cat(1, S.BoundingBox);
FilledAreas = cat(1, S.FilledArea);
pixellist = cat(1,S.PixelList);
PixelIdxList = cat(1,S.PixelIdxList);

[FilledAreas,O] = sort(FilledAreas, 'Descend');

% [~,MaxAreaIndex] = FilledAreas(1,1);
[~,MaxAreaIndex] = max(FilledAreas);
% [~,MaxAreaIndex] = max(FilledAreas(FilledAreas<max(FilledAreas)));

%// Get linear indices of the corresponding silhouette to display along
%// with its bounding box.

imshow(c);
imshow(A,[]);

rectangle('Position', boundingboxes(O(1,1),:), 'Curvature', [0.5 1], 'EdgeColor','r');
rectangle('Position', boundingboxes(O(2,1),:), 'curvature', [0.5 1], 'EdgeColor','r');
rectangle('Position', boundingboxes(O(3,1),:), 'curvature', [0.5 1], 'EdgeColor','r');
rectangle('Position', boundingboxes(O(4,1),:), 'curvature', [0.5 1], 'EdgeColor','r');
rectangle('Position', boundingboxes(O(5,1),:), 'curvature', [0.5 1], 'EdgeColor','r');
rectangle('Position', boundingboxes(O(6,1),:), 'curvature', [0.5 1], 'EdgeColor','r');
rectangle('Position', boundingboxes(O(7,1),:), 'curvature', [0.5 1], 'EdgeColor','r');
rectangle('Position', boundingboxes(O(8,1),:), 'curvature', [0.5 1], 'EdgeColor','r');
rectangle('Position', boundingboxes(O(9,1),:), 'curvature', [0.5 1], 'EdgeColor','r');

% % Find connected components.
% blobAnalysis = vision.BlobAnalysis('AreaOutputPort', true,...
%     'CentroidOutputPort', false,...
%     'BoundingBoxOutputPort', true,...
%     'MinimumBlobArea', 200, 'ExcludeBorderBlobs', true);
% 
% [areas, boxes] = step(blobAnalysis, imDomino);
% 
% % Sort connected components in descending order by area
% [~, idx] = sort(areas, 'Descend');
% 
% % Get the 2 largest components.
% boxes = double(boxes(idx(1:2), :));
% 
% % Adjust for coordinate system shift caused by undistortImage
% boxes(:, 1:2) = bsxfun(@plus, boxes(:, 1:2), newOrigin);
% 
% 
% scale = magnification/100;
% imDetectedshadows = imresize(A, scale);
% 
% % Insert labels for the shadows.
% imDetectedshadows = insertObjectAnnotation(imDetectedshadows, 'rectangle', ...
%         boxes, 'shadows');
% 
% figure; imshow(A);
% title('Detected shadows');
