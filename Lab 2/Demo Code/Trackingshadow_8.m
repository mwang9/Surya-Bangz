% %%Camera Calibration

clear all;
clc;

foto = 'Dominoes (3).jpg';
file = imread(foto);
f=dir(foto);
fil={f.name};  
for k=1:numel(fil);
  file=fil{k};
  new_file=strrep(file,'.jpg','.png');
  im = imread(file);
  imwrite(im,new_file)
end
A = imread('Dominoes (3).png');
imshow(A,[]);

imagefiles = dir('Test/*png');      
numImages = length(imagefiles);    % Number of files found

numImages=1;
files = cell(1, numImages);

for i = 1:numImages
    files{i} = fullfile(matlabroot, 'toolbox', 'vision', 'visiondata', ...
        'calibration', 'slr', sprintf('image%d.png', i));
end

imOrig = imread(fullfile(matlabroot, 'toolbox', 'vision', 'visiondata', ...
        'calibration', 'slr', 'image9.jpg'));

   magnification = 100;   
    figure; imshow(imOrig, 'InitialMagnification', magnification);
title('Input Image');

%% Filter out red colours

A= im2double(A);
I=A;
% range =[10,360];
% 
%     % RGB to HSV conversion
%     I = rgb2hsv(I);
%     
%     % Normalization range between 0 and 1
%     range = range./360;
%     
%     % Mask creation
%     if(size(range,1) > 1), error('Error. Range matrix has too many rows.'); end
%     if(size(range,2) > 2), error('Error. Range matrix has too many columns.'); end
% 
%     if(range(1) > range(2));
%         % Red hue case
%         mask = (I(:,:,1)>range(1) & (I(:,:,1)<=1)) + (I(:,:,1)<range(2) & (I(:,:,1)>=0));
%     else
%         % Regular case
%         mask = (I(:,:,1)>range(1)) & (I(:,:,1)<range(2));
%     end
%     
%     % Saturation is modified according to the mask
%     I(:,:,2) = mask .* I(:,:,2);
%     
%     % HSV to RGB conversion
%     I1 = hsv2rgb(I);
%     imshow(I1);
    
    %% Filter out red (2) colours
% 
% range =[358,350];
% 
%     % RGB to HSV conversion
%     I = rgb2hsv(I1);
%     
%     % Normalization range between 0 and 1
%     range = range./360;
%     
%     % Mask creation
%     if(size(range,1) > 1), error('Error. Range matrix has too many rows.'); end
%     if(size(range,2) > 2), error('Error. Range matrix has too many columns.'); end
% 
%     if(range(1) > range(2));
%         % Red hue case
%         mask = (I(:,:,1)>range(1) & (I(:,:,1)<=1)) + (I(:,:,1)<range(2) & (I(:,:,1)>=0));
%     else
%         % Regular case
%         mask = (I(:,:,1)>range(1)) & (I(:,:,1)<range(2));
%     end
%     
%     % Saturation is modified according to the mask
%     I(:,:,2) = mask .* I(:,:,2);
%     
%     % HSV to RGB conversion
%     I2 = hsv2rgb(I);
%     imshow(I2,[]);
   
%%

range =[5,20];

    % RGB to HSV conversion
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    I = rgb2hsv(I);
    
    % Normalization range between 0 and 1
    range = range./360;
    
    % Mask creation
    if(size(range,1) > 1), error('Error. Range matrix has too many rows.'); end
    if(size(range,2) > 2), error('Error. Range matrix has too many columns.'); end

    if(range(1) > range(2));
        % Red hue case
        mask = (I(:,:,1)>range(1) & (I(:,:,1)<=1)) + (I(:,:,1)<range(2) & (I(:,:,1)>=0));
    else
        % Regular case
        mask = (I(:,:,1)>range(1)) & (I(:,:,1)<range(2));
    end
    
    % Saturation is modified according to the mask
    I(:,:,2) = mask .* I(:,:,2);
    
    % HSV to RGB conversion
    I3 = hsv2rgb(I);
    imshow(I3,[]);
    title('I3');
    
%% Segment/track the shadows

% Convert the image to the HSV color space.
imHSV = rgb2hsv(I3);

% Get the saturation channel.
saturation = imHSV(:, :, 1);

% Threshold the image
t = graythresh(saturation);
imDomino = (saturation > t);

figure; imshow(imDomino);
title('Segmented shadows');

%% 
BW = imDomino;

imshow(BW);

% se = strel('line',5,60);        
% dilateddBW = imdilate(BW,se);

% sizeBW = size(BW);
% 
% k = ones(sizeBW);
% c = xor(k,dilateddBW);
% c = xor(k,BW);
c = BW;

%// Get the region properties and select that with the largest area.
S = regionprops(c,'BoundingBox','FilledArea','b','PixelList','Orientation','PixelIdxList');

imshow(c);
boundingboxes = cat(1, S.BoundingBox);
FilledAreas = cat(1, S.FilledArea);
pixellist = cat(1,S.PixelList);
PixelIdxList = cat(1,S.PixelIdxList);

[FilledAreas,O] = sort(FilledAreas, 'Descend');

% [~,MaxAreaIndex] = FilledAreas(1,1);
[~,MaxAreaIndex] = max(FilledAreas);
% [~,MaxAreaIndex] = max(FilledAreas(FilledAreas<max(FilledAreas)));

%// Get linear indices of the corresponding silhouette to display along
%// with its bounding box.

imshow(A,[]);

amountboxes = max(size(boundingboxes(:,1)));
i=1;

for i = i : 1 : amountboxes;
    
    rectangle('Position', boundingboxes(O(i,1),:), 'Curvature', [0.5 1], 'EdgeColor','r');
    i = i+1;
end


% % Find connected components.
% blobAnalysis = vision.BlobAnalysis('AreaOutputPort', true,...
%     'CentroidOutputPort', false,...
%     'BoundingBoxOutputPort', true,...
%     'MinimumBlobArea', 200, 'ExcludeBorderBlobs', true);
% 
% [areas, boxes] = step(blobAnalysis, imDomino);
% 
% % Sort connected components in descending order by area
% [~, idx] = sort(areas, 'Descend');
% 
% % Get the 2 largest components.
% boxes = double(boxes(idx(1:2), :));
% 
% % Adjust for coordinate system shift caused by undistortImage
% boxes(:, 1:2) = bsxfun(@plus, boxes(:, 1:2), newOrigin);
% 
% 
% scale = magnification/100;
% imDetectedshadows = imresize(A, scale);
% 
% % Insert labels for the shadows.
% imDetectedshadows = insertObjectAnnotation(imDetectedshadows, 'rectangle', ...
%         boxes, 'shadows');
% 
% figure; imshow(A);
% title('Detected shadows');
