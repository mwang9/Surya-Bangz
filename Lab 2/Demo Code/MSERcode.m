function DominoID = findall(dom_face_pic)
%% Description
% Takes in the name of an image file (tif,png,jpg.. doesn't matter) which
% must be in the same path. Returns a 2x1 array of the values on the domino

%% Input
%environment dependent varibles
filter=2;
edgethresh=0.09;

I=imread(dom_face_pic);
%Ig=imadjust(histeq(rgb2gray(I)));
Ig=imadjust(I);
Igf=imgaussfilt(Ig,filter); %filter shit
Igfe=edge([Igf],'Canny',edgethresh); %edge image
se = strel('line',3,3);
imdil = imdilate(Igfe,se);




%% Find lines from Hough Transform peaks
[H,T,R] = hough(imdil,'RhoResolution',0.4,'ThetaResolution',0.4);

P  = houghpeaks(H,10,'threshold',ceil(0.6*max(H(:))));

lines = houghlines(imdil,T,R,P,'FillGap',30,'MinLength',12);
%lines(13)

imlen = length(Igfe);
rm = [];
linesx = [];
tthresh = 5;
for k = 1:length(lines)
    linetheta = lines(k).theta;
    linex = lines(k).point1(1);
    if linetheta<-tthresh || linetheta>tthresh || any(linesx == linex)
        rm = [rm k];
    end
    linesx = [linesx linex];
end
if ~isempty(rm)
   lines(rm) = [];
end
numlines = length(lines);
    
%% Find midpoint line
defaultmidline = imlen*0.325;
if ~numlines
    midpoint = defaultmidline;
else
    midptsum = 0;
    for i = 1:numlines
        midptsum = midptsum + lines(i).point1(1);
    end
    midpoint = midptsum/numlines;
end
    
[regions,mserCC] = detectMSERFeatures((Ig),'RegionAreaRange',[10 35]);
%%
% Show all detected MSER Regions.
figure
imshow(Ig)
hold on
plot(regions,'showPixelList',true,'showEllipses',false)

%%
% Measure the MSER region eccentricity to gauge region circularity.
stats = regionprops('table',mserCC,'Eccentricity');
%%
% Threshold eccentricity values to only keep the circular regions. (Circular regions have low eccentricity.)
eccentricityIdx = stats.Eccentricity < 0.99;
circularRegions = regions(eccentricityIdx);
dotlocs = uniquetol(circularRegions.Location,0.01,'ByRows',true)
plot(dotlocs(:,1),dotlocs(:,2),'b*');
DominoID = [length(dotlocs(:,1)<midpoint); length(dotlocs(:,1)>midpoint)]

%%
% Show the circular regions.
figure
imshow(Ig)
hold on
plot(circularRegions,'showPixelList',true,'showEllipses',false)

corners = detectHarrisFeatures(Igf);

end