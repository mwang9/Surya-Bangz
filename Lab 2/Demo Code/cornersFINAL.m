
function [SQUARES] = cornersclean(picture,back);
%clear all
close all
warning('off','all')
      passlength=0;
passmidind=0;
    changegroup=0;
PASSnsame=0;
MIDLINEPARA=[];
%environment dependent varibles
filter=1.8;
edgethresh=0.4;

%filter=1.2;
%edgethresh=2.0;

%minlength=6;
%minlength=25;
%fillgap=5;
minlength=30;
fillgap=6;
%minlength=70;
%fillgap=21;
nlines=1000;
%nlines=50;

%con_dis=9;
con_dis=6;
not_con_dis=70;
mid_line_dis=100;
para_dis_tol=130;
%mid_line_dis=300;
%para_dis_tol=1000;
%para_angle_tol=7;
para_angle_tol=20;
%plus perp angle
para_group_tol1=0.75;
para_group_tol2=0.4;
dom_mid_dis=1.3;
dom_mid_angle=30;

pic=imread(picture);
pic=pic;%-back;
%pic=imread(picture);
greydpic=double(rgb2gray(pic));
greypic=imgaussfilt(greydpic, filter); %filter shit
edgepic=edge([greypic],'Canny', edgethresh); %edge image
se = strel('line',4,4);
edgepic = imdilate(edgepic,se);


figure(11)
imshow(edgepic);


[H,T,R] = hough(edgepic,'RhoResolution',0.5,'ThetaResolution',0.5);

    
P  = houghpeaks(H,nlines,'threshold',ceil(0.1*max(H(:))));

lines = houghlines(edgepic,T,R,P,'FillGap',fillgap, 'MinLength',minlength);
%{

for l=1:length(lines)
    if (norm(lines(l).point1 - lines(l).point2))>100
        lines(l).point2=[0, 0];
        lines(l).point1=[0, 0];
    end
end
%}

%plot all lines found and hold...
figure(1000), imshow(pic), hold on
max_len = 0;
for k = 1:length(lines)
   xy = [lines(k).point1; lines(k).point2];
   plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');

end


angle_tol1=30; %angle tolerance for parrallel
angle_tol2=angle_tol1;

% filter out concurrent lines 
for k=1:length(lines)
    for j=k+1:length(lines)
        
        if abs(lines(k).theta-lines(j).theta)<angle_tol1 || abs(lines(k).theta +180-lines(j).theta)<angle_tol1 || abs(lines(k).theta-180-lines(j).theta)<angle_tol1;  %check parrallel
            con_angle=rad2deg(atan2(lines(k).point1(2)-lines(j).point1(2),lines(k).point1(1)-lines(j).point1(1)))-90;
               halk= [((lines(k).point1(1) + lines(k).point2(1))/2) ((lines(k).point1(2) + lines(k).point2(2))/2)] ;
               halj= [((lines(j).point1(1) + lines(j).point2(1))/2) ((lines(j).point1(2) + lines(j).point2(2))/2)] ;

            if (abs(con_angle-lines(k).theta)<angle_tol2 || abs(con_angle-180-lines(k).theta)<angle_tol2 || abs(con_angle+180-lines(k).theta)<angle_tol2 || norm(lines(k).point1-lines(j).point1)<con_dis) && norm(halk-halj)<not_con_dis ;%|| norm(lines(k).point2-lines(j).point2)<con_dis;
                if (norm(lines(k).point1 - lines(k).point2) > (norm(lines(j).point1 - lines(j).point2)))
                   lines(j).point1=[0, 0];
                    lines(j).point2=[0, 0];
                else 
                   lines(k).point2=[0, 0];
                   lines(k).point1=[0, 0];
            end
            end
        end
    end
end


%plot dots of unfiltered lines
for k = 1:length(lines)
   xy = [lines(k).point1; lines(k).point2]; 
   % Plot beginnings and ends of lines
 plot(xy(1,1),xy(1,2),'x','LineWidth',2,'Color','yellow');
  plot(xy(2,1),xy(2,2),'x','LineWidth',2,'Color','red');
axis on
grid on
grid minor
end


%calculate line lengths and halfway point 
 len=[];
half=[];
for k=1:length(lines)
 leng = norm(lines(k).point1 - lines(k).point2);
   hal= [((lines(k).point1(1) + lines(k).point2(1))/2) ((lines(k).point1(2) + lines(k).point2(2))/2)] ;
  len=[len leng];
  half=[half ;hal];

end



  EX_LINE=[]; %extended lines
  EX_LINE2=[]; %perpendicular set of extended lines
  M=[]; %line vector
  x=linspace(-size(pic,2),size(pic,2));
  
  %extend lines
  changey=[];
  changex=[];
   for k=1:length(lines)
       
       if lines(k).point1(1)==lines(k).point2(1) && lines(k).point1(1)~=0
          lines(k).point1(1)=lines(k).point2(1)-1;
          changex=[changex k];
       end
         if lines(k).point1(2)==lines(k).point2(2) && lines(k).point2(1)~=0
          lines(k).point1(2)=lines(k).point2(2)-1;
                    changey=[changey k];

       end
       
       m=lines(k).point2-lines(k).point1;
      
       p1=lines(k).point1+m*-10000;
       p2=lines(k).point1+m*+10000;
       coefficients = polyfit([p1(1), p2(1)], [p1(2), p2(2)], 1);
        a1 = coefficients (1);
        a2=-1/a1;
        b = coefficients (2);
        %if a1==0
         %  y1=lines(k).point2(2);
         %  y2=
           
         y1=a1.*x+b;
         y2=a2.*x+300;
         
        EX_LINE=[EX_LINE ; y1];
           EX_LINE2=[EX_LINE2 ; y2];
   end

       %find intersects of extended lines
  
   for k=1:length(lines)
         INTERSECTS=[];
    MID_DIS=[];
    MID_V=[];
     for  j=1:length(lines)
        lin_dis=(sqrt((half(k,1)-half(j,1))^2+ (half(k,2)-half(j,2))^2));
         para_an=lines(j).theta -lines(k).theta;
           inter=InterX([x;EX_LINE(k,:)],[x;EX_LINE(j,:)]);
          if isempty(inter)==0 && k~=j && lin_dis<mid_line_dis && ((para_an<120 && para_an>60) || (para_an>-120 && para_an<-60)) 
              INTERSECTS=[INTERSECTS  inter];
          mid_dis=(sqrt((half(k,1)-inter(1))^2+ (half(k,2)-inter(2))^2));
          MID_DIS=[MID_DIS mid_dis];
 mid_v=[half(k,1)-inter(1) half(k,2)-inter(2)];
 MID_V=[MID_V ; mid_v];
          else 
                        MID_DIS=[MID_DIS 100000];
 MID_V=[MID_V; [0 0]];
           INTERSECTS=[INTERSECTS  [0; 0]];
          end
     end
     [dis_s Js]=sort(MID_DIS);
   
if length(MID_DIS)>1
    L=0;
    M=0;
    new_len=1;
    for l=2:length(MID_DIS)
        for m=1:length(MID_DIS)-1;
                if l>m
   p1=transpose([INTERSECTS(:,Js(m))]);
if abs(norm(p1-half(k,:)))>=abs(len(k)/2-1)
    M=m;
    break
end
                end
            end
           
            if M~=0

   
if abs(MID_V(Js(l),1)+MID_V(Js(M),1))<abs(MID_V(Js(l),1)) && abs(MID_V(Js(l),2)+MID_V(Js(M),2))<abs(MID_V(Js(l),2))

  p2=transpose([INTERSECTS(:,Js(l))]);
if abs(norm(p2-half(k,:)))>=abs(len(k)/2-1)
    L=l;
if M~=0;
    lines(k).point1=transpose([INTERSECTS(:,Js(M))]);
end
if L~=0;
  lines(k).point2=transpose([INTERSECTS(:,Js(L))]);
end
 
 break
      end
  end
end
    end
    
    end
   end

   %{
   for l=1:length(lines)
    if (norm(lines(l).point1 - lines(l).point2))<15
        lines(l).point2=[0, 0];
        lines(l).point1=[0, 0];
    end
   end
%}

%calculate line lengths and halfway point 
 len=[];
half=[];
for k=1:length(lines)
 leng = norm(lines(k).point1 - lines(k).point2);
   hal= [((lines(k).point1(1) + lines(k).point2(1))/2) ((lines(k).point1(2) + lines(k).point2(2))/2)] ;
  len=[len leng];
  half=[half ;hal];

end

INTERSECTS=transpose(INTERSECTS);
%{
%plot all lines found and hold...
figure(9), imshow(pic), hold on
max_len = 0;
for k = 1:length(lines)
   xy = [lines(k).point1; lines(k).point2];
   plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');
   x=[lines(k).point1(1)/1920 lines(k).point2(1)/1920];
   
   y=[1-lines(k).point1(2)/1080 1-lines(k).point2(2)/1080];

   annotation('textarrow',x,y,'String',k)

end

%}
  x=linspace(-size(pic,2),size(pic,2));

para=[]; %array of parrallel groupings
angle_tol=para_angle_tol; %angle tolerance for parrallel
dis_tol=para_dis_tol;   %distanc tolerancet

midline_count=0;
MID_RHO=[];
MID_ANGLE=[];
MID_HAL=[];
MID_INDEX=[];
PARA=[];
PARAL=[];
PARAsort=[];
NSAME=[];
%group parrall lines, and filter unwanted parts
for k=1:length(lines)
  %{
    con=1
   for h=1:length(PARA)
       if k==PARA(h)
            con=0
       end
end
%}
    para=[k];
    PARA_DIS=[0];
    parasort=[];
    for j=1:length(lines)
        if j~=k %&& con==1
          if abs(lines(k).theta-lines(j).theta)<angle_tol %check if parrallel
          para_dis=sqrt((half(k,1)-half(j,1))^2+ (half(k,2)-half(j,2))^2);
             if para_dis<dis_tol;
             
          
                if lines(j).point1(1)~=0 && lines(k).point1(1)~=0; %filter deleted lines
         PARA_DIS=[PARA_DIS para_dis];
                    para=[para j];
                end
             else

          end
          else 
          end
          
        end
    end
     if length(para)>=2
                      PARA=[PARA para ];
                      [PARA_DIS,PARA_DISI]=sort(PARA_DIS);
                      for g=1:length(para)
                          parasort(g)=para(PARA_DISI(g));
                      end
                      antiparasort=fliplr(parasort);
                     % parasort=sort(para);
                     % PARAsort=[PARAsort parasort]
                     PARAL=[PARAL para  0];
     end
     
     
     
     if length(para)>2
         
         for g=1:length(para)
            for f=1:length(para)
         para_dis=sqrt((half(parasort(g),1)-half(antiparasort(f),1))^2+ (half(parasort(g),2)-half(antiparasort(f),2))^2);

                if para_dis>dis_tol
             [para_delete,para_delete_index] = find(para==antiparasort(f));
             para(para_delete_index)=[];
                end
            end
         end
         %{
         nsame=0;
         for u=1:length(PARAsort)-length(para)
             isequall=PARAsort(u:u+length(parasort));
             if isequaln(parasort,isequall)==1
                 PASSnsame=1;
             nsame=nsame+1;
             end
         end 
         NSAME=[NSAME nsame];
         if nsame>1
             %}
             
           dist=[] ; 
              mid_ind=1000;
DI=[]

    for p=1:length(para);
        dis=0;
        interp=InterX([x;EX_LINE(para(p),:)],[x;EX_LINE2(para(p),:)]);
        di=[];
div=[];
    for m=1:length(para)
        %if m~=p
       interm=InterX([x;EX_LINE(para(m),:)],[x;EX_LINE2(para(p),:)]);
       DIl=(sqrt((half(para(m),1)-half(para(p),1))^2+ (half(para(m),2)-half(para(p),2))^2));

%DIl=sqrt((interm(1)-interp(1))^2+ (interm(2)-interp(2))^2);
%DIv=[interm(1)-interp(1) interm(2)-interp(2)];
       DIv=[half(para(m),1)-half(para(p),1) half(para(m),2)-half(para(p),2)];

    div=[div ; DIv];
    di=[di  DIl];

       dis=dis+DIl;
       % end
    end
     %div=sort(div)
   [di diind]=sort(di);
DI=[DI;di];
   for g=3:length(di)
     %  angle_mid=arctan(
     %  if abs(arctan
            if abs(div(diind(g-1),1)+div(diind(g),1))<abs(div(diind(g-1),1)) && abs(div(diind(g-1),2)+div(diind(g),2))<abs(div(diind(g-1),2))
%if abs(div(g-1,1)+div(g,1))<abs(div(g-1,1)) && abs(div(g-1,2)+div(g,2))<abs(div(g-1,2))
           if (di(g-1)/di(g))>para_group_tol1
      passlength=passlength+1;
      
       if length(di)>g
          if di(g)/di(g+1)<para_group_tol2
%checkshadow
              break
          else 
          
                    mid_ind=p;
                                dis=[NaN];
                               break

          end
       elseif length(di)<=g
                    mid_ind=p;
                                dis=[NaN];
                                break

       end
       
       end
       end
   end

            dist=[dist dis];
    end
    
    %{
    display(dist)
     [min_val] =min(dist,[], 'omitnan');
          [max_val] =max(dist,[], 'omitnan');
min_ind=find(dist==min_val);
max_ind=find(dist==max_val);
display(mid_ind)
for p=1:length(dist);
    if p ~=min_ind && p~=max_ind && p~=mid_ind;
        display('cut')
        display(para(p))
        PARAL(k,p)=0;
      % lines(para(p)).point1=[0, 0];
          %         lines(para(p)).point2=[0, 0];
        end
end
    %}
if mid_ind~=1000
    
    
    brake=0;
    for a=1:length(MID_INDEX)
        if para(mid_ind)==MID_INDEX(a)
            brake=1;
        end
    end
       if brake==0 
               MIDLINEPARA=[MIDLINEPARA para 0]

    mid_rho=lines(para(mid_ind)).rho;
   mid_angle=lines(para(mid_ind)).theta;
  mid_hal= [((lines(para(mid_ind)).point1(1) + lines(para(mid_ind)).point2(1))/2) ((lines(para(mid_ind)).point1(2) + lines(para(mid_ind)).point2(2))/2)] ;
MID_HAL=[MID_HAL ; mid_hal];
   MID_ANGLE=[MID_ANGLE mid_angle];
   MID_RHO=[MID_RHO mid_rho];
    midline_count=midline_count+1;
    
    MID_INDEX=[MID_INDEX para(mid_ind)];
    passmidind=passmidind+1;
    midmid=[]
    for g=1:length(para)
        if g~=mid_ind
        midmid_dis=sqrt((half(para(g),1)-mid_hal(1))^2+ (half(para(g),2)-mid_hal(2))^2);
        % midmid=[midmid norm(mid_hal-half(para(g)))]
              midmid=[midmid midmid_dis]
        if midmid_dis<10

       % if norm(mid_hal-half(para(g)))<20
            
            lines(para(g)).point1=[0,0];
                lines(para(g)).point2=[0,0];
        end
        end
    end
       end
end
end
end
%end

%plot all lines found and hold...
figure(40), imshow(pic), hold on
max_len = 0;
for k = 1:length(MID_INDEX)
   xy = [lines(MID_INDEX(k)).point1; lines(MID_INDEX(k)).point2];
   plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');
   
   
%if (x>0 && x<1 && y>0 && y<1)
%   annotation('textarrow',x,y,'String',k)
%end
end

%kill non parrall lines
dont_kill=0;
for g=1:length(lines)
    dont_kill=0;
    for h=1:length(PARA)
        if g==(PARA(h))
            dont_kill=1;
        end
    end
    if dont_kill==0
         lines(g).point1=[0, 0];
                    lines(g).point2=[0, 0];
    end
end
%{
%plot all lines found and hold...
figure(10), imshow(pic), hold on
max_len = 0;
for k = 1:length(lines)
   xy = [lines(k).point1; lines(k).point2];
   plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');
   
   x=[lines(k).point1(1)/1920 lines(k).point2(1)/1920];
   
   y=[1-lines(k).point1(2)/1080 1-lines(k).point2(2)/1080];
%if (x>0 && x<1 && y>0 && y<1)
   annotation('textarrow',x,y,'String',k)
%end
end
%}

 

  %calculate line lengths and halfway point 
 len=[];
half=[];
for k=1:length(lines)
 leng = norm(lines(k).point1 - lines(k).point2);
   hal= [((lines(k).point1(1) + lines(k).point2(1))/2) ((lines(k).point1(2) + lines(k).point2(2))/2)] ;
 %halv=[interm(1)-interp(1) interm(2)-interp(2)];

   len=[len leng];
  half=[half ;hal];

end


%find groups of dominos
  x=linspace(-size(pic,2),size(pic,2));

DOM_GROUP=[];
      MID_INTER=[];
KILL_MID=[];
  for l=1:midline_count
      MID_DIS=[10000 10001 10002 10003];
     MID_V=[10000 10000;10001 10001 ; 10002 10002;10003 10003];
     MID_AN=[0 0 0 0];
      dom_group=[];
      direction=[];
      for s=1:length(lines)
          dir=[0 0];
          if lines(s).point1(1)~=0 && s~=MID_INDEX(l);
             % display(s);
          par_an=lines(s).theta-MID_ANGLE(l);
     mid_dis=(sqrt((half(s,1)-MID_HAL(l,1))^2+ (half(s,2)-MID_HAL(l,2))^2));
 mid_v=[half(s,1)-MID_HAL(l,1) half(s,2)-MID_HAL(l,2)];
 an=atan2(mid_v(2),mid_v(1));
%if (par_an<30 && par_an>-30) ||(par_an<210 && par_an>150)||(par_an>-210 && par_an<-170) || (par_an<120 && par_an>60) || (par_an>-120 && par_an<-60)
if mid_dis<150;
false=0;
for e=1:4
    len_v=sqrt((mid_v(1)-MID_V(e,1))^2+ (mid_v(2)-MID_V(e,2))^2);
an_dif=abs(rad2deg(abs(MID_AN(e)-an)));
    if len_v < (MID_DIS(e)*dom_mid_dis) && (an_dif<dom_mid_angle || an_dif>(360-dom_mid_angle))
    %    if  (an_dif<dom_mid_angle || an_dif>(360-dom_mid_angle))
LEN_V=len_v;
MI_DIS=MID_DIS(e)*dom_mid_dis;
AN_DIF=an_dif;
        % if (an_dif<40 || an_dif>320)
    false=1;
    changegroup=changegroup+1;
       % display(len_v);
       % display(e)
        %display(MID_DIS)
        
        if (mid_dis < MID_DIS(e)) && mid_dis~=0 
           MID_DIS(e)=mid_dis;
                  MID_V(e,:)=mid_v;
                   MID_AN(e)=an;
            %  if abs(lines(s).rho-MID_RHO(l))<rho_tol
                  dom_group(e)=s;
                  break
        end
    break
    end
end

              [M,I]=max(MID_DIS);
              if mid_dis<M  && mid_dis~=0 && false==0
                  MID_DIS(I)=mid_dis;
                  MID_V(I,:)=mid_v;
                    MID_AN(I)=an;
            %  if abs(lines(s).rho-MID_RHO(l))<rho_tol
                  dom_group(I)=s;
              %display(MID_DIS)
           %   end
          end
          end
          end
      end
      killgroup=0;
      for d=1:length(dom_group)
          if dom_group(d)==0
              killgroup=1;
          end
      end

      if killgroup==1
         KILL_MID=[KILL_MID l]


      else
                            DOM_GROUP=[DOM_GROUP ; dom_group];

      
      
    %{  
      %plot all lines found and hold...
figure(100+l), imshow(pic), hold on
max_len = 0;
for b = 1:length(dom_group)
    if dom_group(b)~=0
   xy = [lines(dom_group(b)).point1; lines(dom_group(b)).point2];
   plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');
   
    end
end
      
   %}   
      for y=1:4
         par_an=lines(dom_group(y)).theta-MID_ANGLE(l);
if ~(par_an<10 && par_an>-10) && ~(par_an<190 && par_an>170)&&~(par_an>-190 && par_an<-170)
       inter=InterX([x;EX_LINE(dom_group(y),:)],[x;EX_LINE(MID_INDEX(l),:)]);
       if isempty(inter)==0
       if  inter(1)>0 && inter(1)<1920 && inter(2)>0 && inter(2)<1080
           
MID_INTER=[MID_INTER ; transpose(inter)];
       end
                   end
  end
      end
  end
  end
  

  midline_count=midline_count-length(KILL_MID)
  
  %{
         %MID_HAL(KILL_MID(l),:)=0;
         MID_HAL(KILL_MID(l))=[]
   MID_ANGLE(KILL_MID(l))=[];
   MID_RHO(KILL_MID(l))=[];
    
    MID_INDEX(KILL_MID(l))=[];
        MID_INTER(KILL_MID(l))=[];
  end
  %}
dom_corn=[];
  for l=1:midline_count
      %{
      cont=1;
      for k=1:length(KILL_MID)
          if l==KILL_MID(k)
              cont=0;
          end
      end
      if cont==1
      %}
  EX_LINE=[];
  EX_LINE2=[];
  M=[];
  x=linspace(-size(pic,2),size(pic,2));
  
  %extend lines again 
   for k=1:4
       m=lines(DOM_GROUP(l,k)).point2-lines(DOM_GROUP(l,k)).point1;
       p1=lines(DOM_GROUP(l,k)).point1+m*-1000;
       p2=lines(DOM_GROUP(l,k)).point1+m*+1000;
       coefficients = polyfit([p1(1), p2(1)], [p1(2), p2(2)], 1);
        a1 = coefficients (1);
        a2=-1/a1;
        b = coefficients (2);
         y1=a1.*x+b;
         y2=a2.*x;
        EX_LINE=[EX_LINE ; y1];
           EX_LINE2=[EX_LINE2 ; y2];

        
   end

  
     %find intersects again
    INTERSECTS3=[];
   for k=1:4
     for  j=k+1:4
        
           inter=InterX([x;EX_LINE(k,:)],[x;EX_LINE(j,:)])
           if isempty(inter)==0
         %inter_mid_dis=norm(MID_HAL(l)-half(dom_group(y)))
            inter_dis=norm(lines(DOM_GROUP(l,k)).point1-transpose(inter));
              if inter_dis<150 && inter(1)>0 && inter(1)<1920 && inter(2)>0 && inter(2)<1080
          INTERSECTS3=[INTERSECTS3 ; transpose(inter)];
              end
           end
     end
   end
   dom_corn=[dom_corn ; INTERSECTS3 ];
  end
%  end
   %plot final dots
  
   
   figure(4), imshow(pic), hold on
max_len = 0;
  for l=1:midline_count
     %   cont=1;
     % for k=1:length(KILL_MID)
      %    if l==KILL_MID(k)
       %       cont=0;
        %  end
     % end
     % if cont==1
       
for k = 1:length(DOM_GROUP)
 
     plot(dom_corn(k+(l-1)*4,1),dom_corn(k+(l-1)*4,2),'x','LineWidth',2,'Color','green');

axis on
 end
      end
 % end
  
  SQUARES=[];
 for l=1:midline_count
     %{
       cont=1;
      for k=1:length(KILL_MID)
          if l==KILL_MID(k)
              cont=0;
          end
      end
     if cont==1
     %}
     jump=(l-1)*4;
change=0;
top_x=[2+jump];
bottom_x=[1+jump];
top_y=[1+jump]% 5];
bottom_y=[3+jump]% 6];
%INTERSECTS=transpose(INTERSECTS);
for j=1:4
    for k=(l-1)*4+1:4+(l-1)*4
       
  if (dom_corn(k,1)<dom_corn(bottom_x,1)) 
     
      bottom_x=k;
  
      end
    if dom_corn(k,1)>dom_corn(top_x,1) 
      top_x=k;
  
    end
       if dom_corn(k,2)<dom_corn(bottom_y,2)
      bottom_y=k;
      
      end
       
       if dom_corn(k,2)>dom_corn(top_y,2) 
      top_y=k;
 
       end
    end    
  end    
len1=norm(DOM_GROUP(bottom_y)-DOM_GROUP(top_x))
len2=norm(DOM_GROUP(bottom_y)-DOM_GROUP(bottom_x))
change=0;
if len1>len2
    change=1;
end
    
    SQUARE=[1 1;2 2 ; 3 3; 4 4; 5 5; 6 6];
    
    %if change==0
for k=(l-1)*4+1:4+(l-1)*4
if k==bottom_y
    if change==0
    SQUARE(1,:)=dom_corn(k,:);
    else 
  SQUARE(2,:)=dom_corn(k,:);

    end

elseif k==top_x
     if change==0
    SQUARE(2,:)=dom_corn(k, :);
     else 
  SQUARE(3,:)=dom_corn(k,:);

    end
    elseif k==top_y
         if change==0
    SQUARE(3,:)=dom_corn(k, :); 
   else 
  SQUARE(4,:)=dom_corn(k,:);

    end

elseif k==bottom_x
     if change==0
    SQUARE(4,:)=dom_corn(k,:); 
     else 
  SQUARE(1,:)=dom_corn(k,:);

    end
end

end
 SQUARE(5,:)=MID_INTER(2*l-1,[1 2])  
  SQUARE(6,:)=MID_INTER(2*l,[1 2])      

display(SQUARE)
SQUARES=[SQUARES ; SQUARE];
 end
% end
 display(SQUARES)
 
end