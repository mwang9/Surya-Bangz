function Imlist = egImagelistmaker(numImages)
%% Description
% Makes a cell array (called Imlist) of the image file names (skew1_d#.tif)
% included in this zip file, which were used for testing
% of isFullset.

Imlist = cell(1, numImages);
for i = 1:numImages
    %change name 
    Imlist{i} = sprintf('unskewedImage%d.jpg', i);
end
end