%%%%%%%%*********unskew image********$$$$$$$$$$$$$$$
function [Igft] = dominounskew(corners,picture)
% Read in image
I = imread(picture);
Ig = rgb2gray(I);
%domino width and height
domH= 5.08;
domW = 2.54;
input_points = [corners(1,1) corners(1,2) ;corners(2,1) corners(2,2)  ;corners(4,1) corners(4,2) ;corners(3,1) corners(3,2)];
base_points = [0 0 ; 0 domW ; domH 0 ; domH domW];

%% Cropping the image
%crop image
[x,y,z] = size(Ig)
x1 = [corners(1,1), corners(2,1) ,  corners(3,1), corners(4,1) ];
y1 = [corners(1,2), corners(2,2) , corners(3,2) , corners(4,2)];


%set mask usuing the detected corners
mask = uint8(poly2mask(x1, y1, x, y));
newIm = mask.*Ig; 
% Use the selected points to create a recover the projective transform
tform = cp2tform(input_points, base_points, 'projective');
% Transform the masked image 
%Igft = imtransform(newIm, tform, 'XYScale',.02);
%Igft = imtransform(newIm, tform, 'XYScale',[0.02, );
Igft = imtransform(newIm, tform, 'Size',[4000,2000]);
%Igft = imtransform(Ig, tform, 'XYScale', .05);

figure(300)
imshow(Igft)

k = find(Igft);%find all non zero elements - returns positon vals
%first positon
m = size(Igft);
frow = mod(k(1), m(1));
fcol = (k(1) - frow)/m(1);

endd = size(k);
lrow = mod(k(endd(1)), m(1));
lcol = (k(endd(1)) - lrow)/m(1) ;
%xmin ymin width height
%imshow(Igft)

%Igft2 = imcrop(Igft,[fcol frow  (lcol-fcol) (lrow-frow)]);
%Igft2 is the cropped image
%imshow(Igft2);
y = 3; 
end