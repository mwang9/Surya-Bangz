function result = isFullset(dom_pic_list) %Takes in a cell of image names
%% Description
% Takes in a cell array of image file names (an example is created by egImagelistmaker)
% Returns a string notifying whether all dominoes in the recieved image files make up a full set or whether
% there are duplicates/missing dominoes and how many. The list of detected dominoes is also included.
% This returned string can be concatenated with other strings or just printed to the output window by whatever script
% is calling isFullset

%% Recognise number of dominoes detected and in a full set
result = '';
fullsetnum = 28;
dsetnum = length(dom_pic_list);
if dsetnum > fullsetnum
    arb = ' greater than ';
elseif dsetnum < fullsetnum
    arb = ' less than ';
elseif dsetnum == fullsetnum
    arb = ' equal to ';
end

%Add to result status of the set number
result = strcat(result,'Number of dominoes ',arb,' full set!\n');

%% Complex Analysis - Search for and ID extra/missing dominoes
%Create full set to check against
fullset = zeros([2 fullsetnum]);
count = 1;
for i = 1:7
    for j = 1:i
        fullset(:,count) = [(i-1) (j-1)];
        count = count +1;
    end
end

%ID detected dominoes
dset = zeros([2 dsetnum]);
match = zeros([1 fullsetnum]);
for k = 1:dsetnum
    domID = IDdom(dom_pic_list{k});
    dset(:,k) = domID;
    for i = 1:fullsetnum
        dom = fullset(:,i);
        if dom == domID
           match(i) = match(i)+1;
        elseif dom == flipud(domID)
           match(i) = match(i)+1;
        end
    end
end

%Check for missing/extra dominoes
missing = [];
duplicates = [];
dupnum = 0;
for k = 1:length(match)
    mchnum = match(k);
    if ~mchnum
        missing(:,end+1) = fullset(:,k);
    elseif mchnum>1
        duplicates(:,end+1) = [fullset(:,k) mchnum];
        dupnum = dupnum + mchnum;
    end
end

if match == ones(1,fullsetnum)
    result = strcat(result,...
        'Full set identified!');
else
    if any(duplicates(:))
    result = strcat(result,...
        sprintf('%d duplicates identified.',dupnum));
    end
    if any(missing(:))
    result = strcat(result,...
        sprintf('%d missing domino(es).',length(missing)));
    end
end
% dset
% missing
% duplicates
% match
doms = '';
for i = 1:dsetnum
    doms = strcat(doms,sprintf('%d|%d',dset(1,i),dset(2,i)),'\n');
end
result = sprintf(strcat(result,'\nDominoes found:\n',doms));
end