function D = test_camera(totalFrames)

%% Initialize demo variables

% Demo vars
%totalFrames = 5; % Frames read in loop
picdelay = 2;     % delay between pics in seconds

% Performance measure vars
frameTime = []; 
maxFrames = 5; % Frames to wait before updating on performance


%% Initialize color video device
tic;

%Get videoinput (vi) object
colorVid = videoinput('winvideo', 2);
%colourVid = webcam(1)

%Set input settings
colorVid.FramesPerTrigger = 1;  %Only request one frame per trigger call
colorVid.TriggerRepeat = Inf;   %Tell vi object to allow inf trigger calls

set(colorVid,'ReturnedColorSpace','rgb');


%% Set trigger config for vi objects
triggerconfig([colorVid], 'manual');


%% Start vi devices
start([colorVid]);

for i = 1:10
   fprintf('%i\n', 11-i);
   trigger(colorVid);
   getdata(colorVid);
end

% src = getselectedsource(colorVid);
% src.ExposureMode = 'manual';

%% Get and display 200 frames from vi devices
%figure('Position', [100, 100, 1920, 700]);

% There are many ways to plot an image
% 'imshow' tends to be the easiest how ever it is slow
% the following constructs an image object that can
% have its data overwritten directly improving image display
% performance

% Construct Color image subplot
% subplot = @(m,n,p) subtightplot (m, n, p, [0.02 0.02], [0.02 0.02], [0.02 0.02]);
% subplot(1, 3, 1);

% % Setup plot
% set(gca,'units','pixels');
% set(gca,'xlim',[0 255]);
% set(gca,'ylim',[0 255]);
% 
% % Aquire size of video image format
% sz = colorVid.VideoResolution;
% 
% % Construct image display object
% cim = image(...
%     [1 sz(1)],...
%     [1 sz(2)],...
%     zeros(sz(2), sz(1), 3),...
%     'CDataMapping', 'scaled'...
% );
% 
% % Ensure axis is set to improve display
% axis image;

images = {};

% try

    fprintf('GOOOOOOOO!\n');

    for i = 1:totalFrames
        %Capture Warning
        fprintf('%d',i);
        for j = 1:3
            %delay
            pause(picdelay/3);
            fprintf('.');
        end
        %delay
        pause(picdelay/3);
        fprintf('NOW!');
        
        tic;
        %Trigger a frame request
        trigger([colorVid])

        % Get the color frame and metadata.
        [colorIm, colorTime, colorMeta] = getdata(colorVid);

        %grayIm = rgb2gray(colorIm);
        
        images{i} = colorIm;
        
        %imwrite(grayIm, sprintf('domino_track_test_%i.png', i));
        
        % Update data in image display objects
        %set(cim, 'cdata', colorIm); %Color
        
        % Force a draw update
%        drawnow;
        
        
        
        frameTime(end+1) = toc;
        fprintf('*\n');
        if mod(i, maxFrames) == 0
            fprintf(...
                'Processed %d frames (%d/%d) at %f fps (%fs Avg, %fs Max, %fs Min)\n',...
                maxFrames,...
                i,...
                totalFrames,...
                maxFrames/sum(frameTime),...
                mean(frameTime),...
                max(frameTime),...
                min(frameTime)...
            );
            frameTime = [];
        end
    end
    
    for i = 1:totalFrames
       imwrite(images{i}, sprintf('domino_%d.png', i));
       %imwrite(rgb2gray(images{i}), sprintf('test_image_%d.tif', i)); 
    end
%     
% catch ME
%     fprintf('Error thrown processing frames: %s - %s\n', ME.identifier, ME.message);
% end
%if D = 1 successfully an through code
D = 1;

%% Cleanup vi devices
delete([colorVid]);
clear colorVid;

toc;
end