%% Take calibration photos
%hi = test_camera(5);

%% Calibration
numImages = 5;
files = cell(1, numImages-1);
lastpic = sprintf('domino_%d.png', numImages);
for i = 2:numImages
    %change name 
    files{i-1} = fullfile(sprintf('domino_%d.png', i));
end
[imagePoints, boardSize] = detectCheckerboardPoints(files);
% put in size of the square
squareSize = 29; % in millimeters
worldPoints = generateCheckerboardPoints(boardSize, squareSize);
% Calibrate the camera.
cameraParams = estimateCameraParameters(imagePoints, worldPoints);

% Evaluate calibration accuracy.
figure; showReprojectionErrors(cameraParams);
title('Reprojection Errors');
%sets imorig to the last picture aka calibration image 
imOrig = imread(fullfile(lastpic));
%This undistorts the image
im = undistortImage(imOrig, cameraParams, 'OutputView', 'full');
% Detect the checkerboard on the undistorted image
[imagePoints, boardSize] = detectCheckerboardPoints(im);
% Compute rotation and translation of the camera.
[R, t] = extrinsics(imagePoints, worldPoints, cameraParams);
%this gives intrinsics matrix 
Intrinsic = cameraParams.IntrinsicMatrix;

%find origin point 
%use relationship x/f = X/Z
Intrinsic = cameraParams.IntrinsicMatrix;
%find origin point
%use relationship x/f = X/Z
f = Intrinsic(2,2);
x = -imagePoints(boardSize(1)-1)+imagePoints(boardSize(1)-2);
X = squareSize;
origZ = X/(x/f);%this is depth of field in mm
aA = [imagePoints(boardSize(1)*2-1) imagePoints(boardSize(1)*2-1,2)];
Z = [aA];
%converts image points to world points
worldPoints = pointsToWorld(cameraParams,R,t,Z);
origin = [worldPoints, origZ]

%% Take domino photo
%waits till button press to take another photo
disp('Press to take the domino photo!!')  
pause;
disp('Take Photo!!!')
% hi = test_camera(1);
I = rgb2gray(imread('domino_1.jpg'))
regions = detectMSERFeatures(I);
figure; imshow(I); hold o;n;
plot(regions,'showPixelList',true,'showEllipses',false);

% %% CORNERS
% corner=cornersFINAL('domino_1.png')
% 
% %% UNSKEW
% zz=size(corner);
% pointer = zz(1)/6;
% FaceAr = [];
% noD = 0;
% for i=0:6:zz-6
%     noD=noD +1;
%     cor = [];
%     cor = corner(i+1:i+6, 1:2);
%     unskewedImage = dominounskew(cor, 'domino_1.png');
%     imwrite(unskewedImage,sprintf('unskewedImage%d.jpg',i));
%     
% % %% READ WHAT IS ON THE DOMINOS 
% %   DominoID = IDdom(unskewedImage)
% %   FaceAr = vertcat(FaceAr, DominoID)
%  
% 
% end
% dominostring = isFullset(egImagelistmaker(noD));

%% FINDING DISTANCES
zz=size(corner)
worldp = [];
zposAr = [];
orAr = [];
for i=1: pointer
    thing = i*6;
    domx =corner(thing-1)-corner(thing);
    domy = corner(thing-1,2)-corner(thing,2);
    zpos= (sqrt(domx^2+domy^2)/(x/f))/10;
    %orientation 
    or = atand(domy/domx);
    worldp = vertcat(worldp,[uint8(corner(thing-1)),uint8(corner(thing-1,2))]);
    zposAr = vertcat(zposAr, zpos);
    orAr = vertcat(orAr, or);
    
end
wp = pointsToWorld(cameraParams,R,t,worldp);
positionsOfDominos = horzcat(wp, zposAr);

for i=1: pointer
    dompos = positionsOfDominos(i)-origin;
    dompos = horzcat(dompos, orAr(i));
    display(sprintf('domino %d : x   y   z  angle ',i))
    disp(dompos)
    if abs(orAr(i))<45
        orientation = 'vertical';
    else
        orientation = 'horizontal';
    end
    display(sprintf('the domiono is: %s', orientation));
    display(dominostring)
end
