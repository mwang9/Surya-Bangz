%Demo 10
%21-08-2016
%blind - as tested by Rhys & Eliza before demo day

%set origin coordinate
origin = [6 3 0];
orig = [];
for i=1:size(origin,1)
    [A,B,C]=Angle4(origin(i,1),origin(i,2),origin(i,3));
    orig(i,1)=A;
    orig(i,2)=B;
    orig(i,3)=C;
end

%points -> theta
%points = [6 3 0];
points = [6 3 0; 3 3 2; 5 1 3; 1 6 2; 10 6 5; 6 3 0];


theta=[];
for i=1:size(points,1)%length(points)
    [A,B,C]=Angle4(points(i,1),points(i,2),points(i,3));
    theta(i,1)=A;
    theta(i,2)=B;
    theta(i,3)=C;
end
disp(sprintf('Theta array is'));
disp(theta);

%establish connection to NXT
COM_CloseNXT all;
h = COM_OpenNXT();
COM_SetDefaultNXT(h);

%motor parameters
mA = NXTMotor('A', 'ActionAtTachoLimit', 'brake');
mB = NXTMotor('B', 'ActionAtTachoLimit', 'Holdbrake');
mC = NXTMotor('C', 'ActionAtTachoLimit', 'Holdbrake');
m = [mA mB mC];
speed = [25 12 15];

%orig = [398 -4 -127];%[398 -2 -129];
pos = orig;
default = [-140 20];

%initial setup
for j = m
    j.Stop('off');
    j.ResetPosition();
end

%for each point
for i=1:size(theta,1)
    for  j = [4 5 1 3 2]; %motors moved: C B A B C   j=[5 4 1 2 3];
        if j <= 3
            if theta(i,j) >= pos(j)
                m(j).TachoLimit = theta(i,j) - pos(j);
                %disp(sprintf('posA movement is %d',m(j).TachoLimit));
                m(j).Power = speed(j);
            else theta(i,j) < pos(j)
                m(j).TachoLimit = pos(j) - theta(i,j);
                %disp(sprintf('posA movement is %d',-m(j).TachoLimit));
                m(j).Power = -speed(j);
            end
        elseif j == 5
            if default(j-3) >= pos(3)
                m(3).TachoLimit = default(j-3) - pos(3);
                m(3).Power = speed(3);
            else default(j-3) < pos(3)
                m(3).TachoLimit = pos(3) - default(j-3);
                m(3).Power = -speed(3);
            end
        elseif j == 4
            if default(j-3) >= pos(2)
                m(2).TachoLimit = default(j-3) - pos(2);
                m(2).Power = speed(2);
            else default(j-3) < pos(2)
                m(2).TachoLimit = pos(2) - default(j-3);
                m(2).Power = -speed(2);
            end
        else %j == 6
            if pos(2) >= 0
                m(2).TachoLimit = pos(2) + 15;
            elseif pos(2) < 0
                m(2).TachoLimit = 15 - pos(2);
            end
            m(2).Power = -m(2).Power;
        end
           
        if j <= 3
            if m(j).TachoLimit ~= 0
                m(j).SendToNXT();
                m(j).WaitFor();
            end
            data = m(j).ReadFromNXT(); %gives matrix of data
            pos(j) = data.Position + orig(j);
        elseif j == 4 || j == 5
            if m(j-2).TachoLimit ~= 0
                m(j-2).SendToNXT();
                m(j-2).WaitFor();
            end
            data = m(j-2).ReadFromNXT(); %gives matrix of data
            pos(j-2) = data.Position + orig(j-2);
        else
            if m(2).TachoLimit ~= 0
                m(2).SendToNXT();
                m(2).WaitFor();
            end
            data = m(2).ReadFromNXT(); %gives matrix of data
            pos(2) = data.Position + orig(2);
        end
        
        if j <= 3
            while pos(j) ~= theta(i,j)
                error = theta(i,j) - pos(j);
                disp(sprintf('Error is %d',error));
                if abs(error) <= 1
                    data = m(j).ReadFromNXT();
                    pos(j) = data.Position + orig(j);
                    break;
                end
                m(j).TachoLimit = abs(error);
                m(j).Power = sign(error)*speed(j);
                if abs(error) < 5
                    m(j).Power = 4*m(j).Power;
                end
                m(j).SendToNXT();
                m(j).WaitFor();
                data = m(j).ReadFromNXT();
                pos(j) = data.Position + orig(j);
            end         
        elseif j == 4 || j == 5
            while pos(j-2) ~= default(j-3)
                error = default(j-3) - pos(j-2);
                disp(sprintf('DefError is %d',error));
                if j == 4 || abs(error) <= 1
                    data = m(j-2).ReadFromNXT();
                    pos(j-2) = data.Position + orig(j-2);
                    break;
                end
                m(j-2).TachoLimit = abs(error);
                m(j-2).Power = sign(error)*speed(j-2);
                if abs(error) < 5
                    m(j-2).Power = 4*m(j-2).Power;
                end
                m(j-2).SendToNXT();
                m(j-2).WaitFor();
                data = m(j-2).ReadFromNXT();
                pos(j-2) = data.Position + orig(j-2);
            end
        end
        disp(pos);
    end
end

COM_CloseNXT all;
COM_OpenNXT();
COM_CloseNXT all;