%Demo 1

%The angle ie. Tacholimit1 can be used to keep track of the ground plane
%arm position
%Apply trigonometry rules (sin-cos)
%Hypotenuse will be determined by the bending elbow section
%ie. Tacholimit2

%establish connection to NXT
COM_CloseNXT all;
h = COM_OpenNXT();
COM_SetDefaultNXT(h);

%Properties:
%1. Power = Speed
%2. TachoLimit = Angle/Desired Position
%3. ActionAtTachoLimit = Response to reaching TachoLimit
%4. SpeedRegulation = Constant Speed (true) vs. Constant torque (false)

spread = 50;
reach = 10;
speedA = 15;
speedB = 10;
m = NXTMotor('BC');
mA = NXTMotor('A', 'Power', speedA);
mB = NXTMotor('B', 'Power', speedB);%, 'TachoLimit', 9*reach); %+ve power goes up
mC = NXTMotor('C', 'Power', speedB);%, 'TachoLimit', 5*reach); %+ve power goes down

%mA = NXTMotor('Port','Property',value, etc.)
%mA = NXTMotor('ABC', 'Power', speed, 'TachoLimit', spread); %half max power

m.Stop('off');
m.ResetPosition();
%mA.SendToNXT();
mC.TachoLimit = 30;
%mB.ActionAtTachoLimit = 'HoldBrake';
%mB.SendToNXT();
mC.SendToNXT();
%mA.WaitFor();
%mC.SendToNXT();
%mB.WaitFor();
mC.WaitFor();
%mA.ResetPosition();
%mA.TachoLimit = 60;
%mA.SendToNXT();
%mA.WaitFor();
%pause(1000);

%mA.Power = -mA.Power;
%mB.Power = -mB.Power;
%mC.Power = -mC.Power;
%mC.Stop('off');
%mA.SendToNXT();
%mB.SendToNXT();
%mC.SendToNXT();
%mA.WaitFor();

%mB.Power = -mB.Power;
%mB.SendToNXT();
%mB.WaitFor();

%returns current data from NXT (data.Power =/= mA.Power)
%data = mA.ReadFromNXT();
%show data to user
%disp(sprintf('Motor A is currently at position %d',data.position));
%pause(2);
%

mB.Stop('off');
mB.ResetPosition();

COM_CloseNXT all;