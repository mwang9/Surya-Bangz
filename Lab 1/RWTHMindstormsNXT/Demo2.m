%Demo 2 
%
%Example which should move a motor (or robotic arm) 10 times back and
% forth (up and down), as precisely as possible without blindly trusting
% the motor commands. Very simple error compensation (i.e. trying absolute
% movements instead of always relative).
% Prepare MATLAB
COM_CloseNXT all
close all
clear all
% Connect to NXT, via USB or BT
h = COM_OpenNXT();
COM_SetDefaultNXT(h);
% set params
power = 20;
port = MOTOR_B;
dist = 50; % distance to move in degrees
% create motor objects
% we use holdbrake, make sense for robotic arms
mUp = NXTMotor(port, 'Power', -power, 'ActionAtTachoLimit', 'HoldBrake');
mDown = NXTMotor(port, 'Power', power, 'ActionAtTachoLimit', 'HoldBrake');
% prepare motor
mUp.Stop('off');
mUp.ResetPosition();
% repeat 10 times
for j=1:10
% where are we?
data = mUp.ReadFromNXT();
pos = data.Position;
% where do we want to go?
% account for errors, i.e. if pos is not 0
mDown.TachoLimit = dist + pos;
% move
mDown.SendToNXT();
mDown.WaitFor();
% now we are at the bottom, repeat the game:
% where are we?
data = mUp.ReadFromNXT(); % doesn't matter which object we use to read!
pos = data.Position;
disp(sprintf('%d',data.Position));
% pos SHOULD be = dist in an ideal world
% but calculate new "real" distance to move
% based on current error...
mUp.TachoLimit = pos;
% this looks very simple now, but it comes from
% TachoLimit = dist + (pos ? dist);
% i.e. real distance + error correction
% Imagine it this way: We are currently at pos,
% and want to go back to 0, so this is exactly the distance
% to go!
mUp.SendToNXT();
mUp.WaitFor();
end%for

% mode was HOLDBRAKE, so don't forget this:
mUp.Stop('off')
% clean up
COM_CloseNXT(h);