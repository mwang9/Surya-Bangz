%Demo 8
%17-08-2016
%Move 2 motors at once when returning to safe position (default)

%Finding theta
%points = [15 15 95; 160 0 0; 320 0 0];
points = [15 15 95];
theta=[];
for i=1:size(points,1)%length(points)
    [A,B,C]=Angle4(points(i,1),points(i,2),points(i,3));
    theta(i,1)=A;
    theta(i,2)=B;
    theta(i,3)=C;
end
disp(sprintf('Theta array is'));
disp(theta);

%establish connection to NXT
COM_CloseNXT all;
h = COM_OpenNXT();
COM_SetDefaultNXT(h);

%parameters

mA = NXTMotor('A', 'ActionAtTachoLimit', 'brake');
mBC = NXTMotor('BC', 'ActionAtTachoLimit', 'Holdbrake');
mC = NXTMotor('C', 'ActionAtTachoLimit', 'Holdbrake');
%mBC = NXTMotor('BC');
m = [mA mBC mC];
speed = [10 15 10]; %[A B C]
pos = [0 0 0];
default = [0 0 0];

%init_setup
for j = m
    j.Stop('off');
    j.ResetPosition();
end

for i=1:size(theta,1)
    for j=[1 2 3 5 4]
        if j == 1
            %20 to -60 (move -80)
            %60 to -20 (move -80)
            
            %-20 to 60
            %-60 to 20 (move +80)
            
            %30 to 50
            
            %-40 to -90
            if theta(i,j) >= pos(j)
                m(j).TachoLimit = theta(i,j) - pos(j);
                disp(sprintf('posA movement is %d',m(j).TachoLimit));
                m(j).Power = speed(j);
            else theta(i,j) < pos(j)
                m(j).TachoLimit = pos(j) - theta(i,j);
                disp(sprintf('posA movement is %d',-m(j).TachoLimit));
                m(j).Power = -speed(j);
            end
        elseif j == 2 || j == 3 
            if theta(i,j) >= pos(j) 
                m(j).TachoLimit = theta(i,j) - pos(j);
                disp(sprintf('posB/C movement is %d',m(j).TachoLimit));
                m(j).Power = speed(j);
            elseif theta(i,j) < pos(j)
                m(j).TachoLimit = pos(j) - theta(i,j);
                disp(sprintf('posB/C movement is %d',-m(j).TachoLimit));
                m(j).Power = -2*speed(j);
            end
        elseif j == 4
            if default(2) >= pos(2)
                m(2).TachoLimit = default(2) - pos(2);
                disp(sprintf('posBC movement is %d',m(2).TachoLimit));
                m(2).Power = speed(2);
            else default(2) < pos(2)
                m(2).TachoLimit = pos(2) - default(2);
                disp(sprintf('posBC movement is %d',-m(2).TachoLimit));
                m(2).Power = -(speed(2)+5);
            end
        else j == 5
            if default(3) >= pos(3)
                m(3).TachoLimit = default(3) - pos(3);
                disp(sprintf('posC movement is %d',m(3).TachoLimit));
                m(3).Power = speed(3);
            else default(3) < pos(3)
                m(3).TachoLimit = pos(3) - default(3);
                disp(sprintf('posC movement is %d',-m(3).TachoLimit));
                m(3).Power = -speed(3);
            end
        end
        
        disp(sprintf('J is %d',j));
        
        if j <= 3
            if m(j).TachoLimit ~= 0
                m(j).SendToNXT();
                m(j).WaitFor();
            end
            data = m(j).ReadFromNXT();
            pos(j) = data.Position;
        else
            if m(j-2).TachoLimit ~= 0
                m(j-2).SendToNXT();
                m(j-2).WaitFor();
            end
            data = m(j-2).ReadFromNXT();
            pos(j-2) = data.Position;
            if j == 4
                pos(3) = pos(2);
            end
        end
        
        if j <= 3
            while pos(j) ~= theta(i,j)
                error = theta(i,j) - pos(j);
                disp(sprintf('Error is %d',error));
                if abs(error) <= 1
                    break;
                end
                m(j).TachoLimit = abs(error);
                m(j).Power = sign(error)*speed(j);
                if j == 1 & abs(error) < 10
                    m(j).Power = 3*m(j).Power;
                end
                m(j).SendToNXT();
                m(j).WaitFor();
                data = m(j).ReadFromNXT();
                pos(j) = data.Position;
            end
        else
            while pos(j-2) ~= default(j-2)
                error = default(j-2) - pos(j-2);
                disp(sprintf('DefError is %d',error));
                if abs(error) <= 1
                    break;
                end
                m(j-2).TachoLimit = abs(error);
                m(j-2).Power = sign(error)*speed(j-2);
                m(j-2).SendToNXT();
                m(j-2).WaitFor();
                data = m(j-2).ReadFromNXT();
                pos(j-2) = data.Position;
            end
        end
        disp(pos);
    end
end

COM_CloseNXT all;
COM_OpenNXT();
COM_CloseNXT all;