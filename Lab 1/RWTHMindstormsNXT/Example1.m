%Example 1

%The angle ie. Tacholimit1 can be used to keep track of the ground plane
%arm position
%Apply trigonometry rules (sin-cos)
%Hypotenuse will be determined by the bending elbow section
%ie. Tacholimit2

%establish connection to NXT
COM_CloseNXT all;
h = COM_OpenNXT();
COM_SetDefaultNXT(h);

%Properties:
%1. Power = Speed
%2. TachoLimit = Angle/Desired Position
%3. ActionAtTachoLimit = Response to reaching TachoLimit
%4. SpeedRegulation = Constant Speed (true) vs. Constant torque (false)

spread = 50;
reach = 10;
speedA = 100;
speedB = 50;
m = NXTMotor('BC');
mA = NXTMotor('A', 'Power', -speedA);%, 'TachoLimit', 45);
mB = NXTMotor('B', 'Power', -speedB, 'TachoLimit', 4*reach);
mC = NXTMotor('C', 'Power', speedB, 'TachoLimit', 3*reach);

%mA = NXTMotor('Port','Property',value, etc.)
%mA = NXTMotor('ABC', 'Power', speed, 'TachoLimit', spread); %half max power

for j=1:90
    angle = 1
    mA.TachoLimit = angle;
    mA.Stop('off');
    mA.ResetPosition();
    mA.SendToNXT();
    mA.WaitFor();
    angle = angle + 1;
end%for

%mA.Stop('off');
%mA.ResetPosition();
%mA.SendToNXT();
%mA.SendToNXT();
%mC.SendToNXT();
%mA.WaitFor();
%mC.SendToNXT();
%mC.WaitFor();
%mC.WaitFor();

%pause(1000);

mA.Power = -mA.Power;
%mB.Power = -mB.Power;
%mC.Power = -mC.Power;
%mC.Stop('off');
mA.SendToNXT();
%mB.SendToNXT();
%mC.SendToNXT();
mA.WaitFor();

%returns current data from NXT (data.Power =/= mA.Power)
%data = mA.ReadFromNXT();
%show data to user
%disp(sprintf('Motor A is currently at position %d',data.position));
%pause(2);
%

mC.Stop('off');
mC.ResetPosition();

COM_CloseNXT all;