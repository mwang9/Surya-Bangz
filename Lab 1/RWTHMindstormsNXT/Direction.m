%Direction & Position

COM_CloseNXT all;
h = COM_OpenNXT();
COM_SetDefaultNXT(h);

speed = [25];
mA = NXTMotor('A', 'Power', speed(1), 'ActionAtTachoLimit', 'Brake');
mB = NXTMotor('B', 'Power', speed(1), 'ActionAtTachoLimit',  'HoldBrake');
mC = NXTMotor('C', 'Power', speed(1), 'ActionAtTachoLimit', 'HoldBrake');
mBC = NXTMotor('BC', 'Power', speed(1), 'ActionAtTachoLimit');%, 'ActionAtTachoLimit', 'HoldBrake');
m = [mA];% mC mA]; %order of movement
pos = [0]; %(current) position vector
cpos = [];

%angle = 86
%angle = 58
angle = 50;

m(1).TachoLimit = angle;
m(1).SendToNXT();
m(1).WaitFor();

data = m(1).ReadFromNXT();
pos(1) = data.Position;
disp(pos);

data = mB.ReadFromNXT();
cpos(1) = data.Position;
data = mC.ReadFromNXT();
cpos(2) = data.Position;
disp(cpos);

data.Position = 50;
%cpos(2) = data.Position;
disp(cpos);

%m(1).ActionAtTachoLimit = 'Brake';
m(1).Stop('off');

COM_CloseNXT all;
COM_OpenNXT();
COM_CloseNXT all;