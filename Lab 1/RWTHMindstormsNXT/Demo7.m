%Demo 7
%16-08-2016
%Most recent working demo inc. point-to-angle conversion

%Array
points = [15 15 95; 160 0 0; 320 0 0];
theta=[];
for i=1:size(points,1)%length(points)
    [A,B,C]=Angle4(points(i,1),points(i,2),points(i,3));
    theta(i,1)=A;
    theta(i,2)=B;
    theta(i,3)=C;
end
disp(sprintf('Theta array is'));
disp(theta);

%theta = [60 30 -15; 0 0 0; -40 -20 45];
%establish connection to NXT
COM_CloseNXT all;
h = COM_OpenNXT();
COM_SetDefaultNXT(h);

%parameters
speed = [10 7 10]; %[A B C]
mA = NXTMotor('A', 'Power', speed(1), 'ActionAtTachoLimit', 'brake');
mB = NXTMotor('B', 'Power', speed(2), 'ActionAtTachoLimit', 'Holdbrake');
mC = NXTMotor('C', 'Power', speed(3), 'ActionAtTachoLimit', 'Holdbrake');
m = [mA mB mC]; %order of movement
pos = [0 0 0]; %(current) position vector

%init_setup
for j = m
    j.Stop('off');
    j.ResetPosition(); %origin is 0
end 

%for each point
for i=1:size(points,1) 
    %for each coordinate/motor
    for j=1:3
        if j == 1
            disp(theta(i,j) + pos(j));
            if theta(i,j) >= pos(j)
                m(j).TachoLimit = theta(i,j) + pos(j);
                disp(sprintf('posA movement is %d',m(j).TachoLimit));
                m(j).Power = speed(j);
            else theta(i,j) < pos(j)
                m(j).TachoLimit = -theta(i,j) + pos(j);
                disp(sprintf('posA movement is %d',-m(j).TachoLimit));
                m(j).Power = -speed(j);
            end
        else %j = 2 or 3 
            if theta(i,j) >= pos(j) 
                m(j).TachoLimit = theta(i,j) - pos(j);
                disp(sprintf('posB-C movement is %d',m(j).TachoLimit));
                m(j).Power = 0.5*speed(j);
            elseif theta(i,j) < pos(j)
                m(j).TachoLimit = pos(j) - theta(i,j);
                disp(sprintf('posB-C movement is %d',-m(j).TachoLimit));
                m(j).Power = -speed(j);
            end
        end
        
        %disp(sprintf('J is %d',j));
        
        if m(j).TachoLimit ~= 0
            m(j).SendToNXT();
            m(j).WaitFor();
        end
        data = m(j).ReadFromNXT(); %gives matrix of data
        pos(j) = data.Position; %just returns the position of data
        
        %error check
        %{
        while pos(j) ~= theta(i,j)
            %e.g 46(pos) instead of 45(theta) -> move -1, 45-46
            %44 instead of 45 -> move +1, 45-44
            %-44 instead of -45 -> move -1
            %-46 instead of -45 -> move +1
            error = theta(i,j) - pos(j)
            if abs(error) <= 10
                break;
            end
            m(j).TachoLimit = abs(error);
            m(j).Power = sign(error)*speed(j);
            if j == 2 && abs(error) < 10
                m(j).Power = 3*m(j).Power;
            end
            m(j).SendToNXT();
            m(j).WaitFor();
            data = m(j).ReadFromNXT();
            pos(j) = data.Position;
        end
        %}
        disp(pos);
    end
end

COM_CloseNXT all;
COM_OpenNXT();
COM_CloseNXT all;