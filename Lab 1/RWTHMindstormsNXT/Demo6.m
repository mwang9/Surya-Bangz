%Demo 6
%NOTE: mA.TachoLimit CANNOT EQUAL 0...motor will run forever & break gear
%15-08-2016

%Array
points = [150 150 100; 0 -65 350];
theta=[];
for i=1:size(points,1)%length(points)
    [theta1,theta2,theta3]=Demo3(points(i,1),points(i,2),points(i,3));
    theta(i,1)=theta2;
    theta(i,2)=theta3;
    theta(i,3)=theta1;
end
disp(sprintf('Theta array is'));
disp(theta); %[theta2,theta3,theta1]
%[B C A]
%point to theta calc
%theta = [30 45 45; 0 0 0];
%theta = [30; -45; -90];
%theta = [90; 0; 45];
%establish connection to NXT
COM_CloseNXT all;
h = COM_OpenNXT();
COM_SetDefaultNXT(h);

%parameters
speed = [30 -16 16]; %[B C A]
mA = NXTMotor('A', 'Power', speed(3), 'ActionAtTachoLimit', 'brake');
mB = NXTMotor('B', 'Power', speed(1), 'ActionAtTachoLimit', 'Holdbrake');
mC = NXTMotor('C', 'Power', speed(2), 'ActionAtTachoLimit', 'Holdbrake');
m = [mB mC mA]; %order of movement
pos = [0 0 0]; %(current) position vector

% mC.TachoLimit = 5;
% mC.SendToNXT();
% mC.WaitFor();
% mB.TachoLimit = 5;
% mB.SendToNXT();
% mB.WaitFor();

%mB.Stop('Holdbrake');
%mC.Stop('Holdbrake');

%init_setup
for j = m
    j.Stop('off');
    j.ResetPosition(); %origin is 0
end 

%for each point
for i=1:size(points,1)%length(theta)%i=1:6
    % = Demo4(points(i,1),points(i,2),points(i,3));
    
        %for each coordinate/motor
    for j=1:3
        if j == 3
            if theta(i,j) >= pos(j)
                m(j).TachoLimit = theta(i,j) + pos(j);
                %disp(sprintf('posA movement is %d',m(j).TachoLimit));
                m(j).Power = speed(j);
            else theta(i,j) < pos(j)
                m(j).TachoLimit = -theta(i,j) + pos(j);
                %disp(sprintf('posA movement is %d',-m(j).TachoLimit));
                m(j).Power = -speed(j);
            end
        else %j = 1 or 2 
            if theta(i,j) >= pos(j) 
                m(j).TachoLimit = theta(i,j) - pos(j);
                %disp(sprintf('posB movement is %d',m(j).TachoLimit));
                m(j).Power = speed(j);
            elseif theta(i,j) < pos(j)
                m(j).TachoLimit = pos(j) - theta(i,j);
                %disp(sprintf('posB movement is %d',-m(j).TachoLimit));
                m(j).Power = -0.5*speed(j);
            end
        end
        m(j).SendToNXT();
        m(j).WaitFor();
        %pause(0.5);
        data = m(j).ReadFromNXT(); %gives matrix of data
        pos(j) = data.Position; %just returns the position of data
        if j == 2
            pos(j) = -pos(j); 
        end
        
        %error check
        while pos(j) ~= theta(i,j)
            %e.g 46(pos) instead of 45(theta) -> move -1, 45-46
            %44 instead of 45 -> move +1, 45-44
            %-44 instead of -45 -> move -1
            %-46 instead of -45 -> move +1
            error = theta(i,j) - pos(j)
            if abs(error) <= 7
                break;
            end
            m(j).TachoLimit = abs(error);
            m(j).Power = sign(error)*speed(j);
            %if j == 1
            %    m(j).Power = 3*m(j).Power;
            %end
            m(j).SendToNXT();
            m(j).WaitFor();
            %pause(0.25);
            data = m(j).ReadFromNXT();
            pos(j) = data.Position;
            if j == 2
                pos(j) = -pos(j);
            end
        end
        disp(pos);
    end
end

COM_CloseNXT all;