%Demo 5
%NOTE: mA.TachoLimit CANNOT EQUAL 0...motor will run forever & break gear
%12-08-2016

%Array
%[x,y,z]
points = [0 0 0; 20 50 30 ;30 20 50; -40 60 -30];
%point to theta calc
theta = [45 45 45; 1 1 1; 45 -45 -45];
%theta = [30; -45; -90];
%theta = [90; 0; 45];

%establish connection to NXT
COM_CloseNXT all;
h = COM_OpenNXT();
COM_SetDefaultNXT(h);

%parameters
speed = [-26 20 20];
mA = NXTMotor('A', 'Power', speed(1), 'ActionAtTachoLimit', 'Brake');
mB = NXTMotor('B', 'Power', speed(2), 'ActionAtTachoLimit', 'HoldBrake');
mC = NXTMotor('C', 'Power', speed(3), 'ActionAtTachoLimit', 'HoldBrake');
m = [mB mC mA]; %order of movement
pos = [0 0 0]; %(current) position vector

%init_setup
for j = m
    j.Stop('off');
    j.ResetPosition(); %origin is 0
end 

%for each point
for i=1:length(points)
    %[theta1,theta2] = Demo4(points(i,1),points(i,2));
    
    %for each coordinate/motor
    for j=1:3
        if j == 3
            if theta(i,j) >= pos(j)
                m(j).TachoLimit = theta(i,j) + pos(j);
                %disp(sprintf('posA movement is %d',m(j).TachoLimit));
                m(j).Power = speed(j);
            else theta(i,j) < pos(j)
                m(j).TachoLimit = -theta(i,j) + pos(j);
                %disp(sprintf('posA movement is %d',-m(j).TachoLimit));
                m(j).Power = -speed(j);
            end
        else
            if theta(i,j) >= pos(j)
                m(j).TachoLimit = theta(i,j) - pos(j);
                %disp(sprintf('posB movement is %d',m(j).TachoLimit));
                m(j).Power = speed(j);
            elseif theta(i,j) < pos(j)
                m(j).TachoLimit = pos(j) - theta(i,j);
                %disp(sprintf('posB movement is %d',-m(j).TachoLimit));
                m(j).Power = -0.5*speed(j);
            end
        %{    
        else
            if theta(i,j) >= pos(j)
                m(j).TachoLimit = theta(i,j) - pos(j);
                %disp(sprintf('posC movement is %d',m(j).TachoLimit));
                m(j).Power = speed(j);
            elseif theta(i,j) < pos(j)
                m(j).TachoLimit = pos(j) - theta(i,j);
                %disp(sprintf('posC movement is %d',-m(j).TachoLimit));
                m(j).Power = -speed(j);
            end
        %}
        end
        m(j).SendToNXT();
        m(j).WaitFor();
        %pause(0.5);
        data = m(j).ReadFromNXT();
        pos(j) = data.Position;
        if j == 1
            pos(j) = -pos(j);
        end
        
        while pos(j) ~= theta(i,j)
            %e.g 46(pos) instead of 45(theta) -> move -1, 45-46
            %44 instead of 45 -> move +1, 45-44
            %-44 instead of -45 -> move -1
            %-46 instead of -45 -> move +1
            error = theta(i,j) - pos(j)
            if abs(error) == 1
                break;
            end
            m(j).TachoLimit = abs(error);
            m(j).Power = sign(error)*speed(j);
            if j == 1
                m(j).Power = 3*m(j).Power;
            end
            m(j).SendToNXT();
            m(j).WaitFor();
            %pause(0.25);
            data = m(j).ReadFromNXT();
            pos(j) = data.Position;
            if j == 1
                pos(j) = -pos(j);
            end
        end
        disp(pos);
    end
end

COM_CloseNXT all;