%Demo4
%Testing Eliza's function

function [theta1, theta2] = Demo4(posx, posy)

L1 = 122; %armsegment1
L2 = 180; %armsegment2
theta2 = acosd((posx^2 + posy^2 - (L1^2 +L2^2))/(2*L1*L2));
theta1 = acosd((L2* cos(theta2) - posx)/L1);

disp(sprintf('For the coordinate (%d,%d)',posx,posy));
disp(sprintf('Theta1 is %d',theta1));
disp(sprintf('Theta2 is %d',theta2));

end