function [Thetaz,Theta1,Theta2] = Angles2(x, y, z)

x=x*32-16;
y=y*32-16;
z=z*32*(2.5/4);
disp([x,y,z]);

L1 = 120;
L2 = 230;
h = 218;%210;%185;

offsetX = 163;
offsetY = 80;%72;

px = sqrt(((x-offsetX)^2 + (y+offsetY)^2));
py = z-h;

%disp(sprintf('px: %3.1f',px));

k = (px^2 +py^2 - L1^2 - L2^2)/(2*L1*L2);
theta2 = acosd(k);
theta2n = -theta2;

delta = L1^2 + L2^2 + 2*L1*L2*cosd(theta2);
ct1 = (px*(L1 + L2*cosd(theta2))+ py*L2*sind(theta2))/delta;
st1 = (-px*L2*sind(theta2)+ py*(L1+L2*cosd(theta2)))/delta;

deltan = L1^2 + L2^2 + 2*L1*L2*cosd(theta2n);
ct1n = (px*(L1 + L2*cosd(theta2n))+ py*L2*sind(theta2n))/delta;
st1n = (-px*L2*sind(theta2n)+ py*(L1+L2*cosd(theta2n)))/delta;

gearz = 56/8;%58/8;
gearb = 40/24;
gearc = 40/24;%40/20;%42/26;

Thetaz = -round(gearz*atan2d(x-offsetX,y+offsetY));
theta1 = round(-gearb*atan2d(st1, ct1));
theta1n = round(-gearb*atan2d(st1n, ct1n));
theta2 = round(theta2*(gearc) - theta1);
theta2n = round(theta2n*(gearc) - theta1n);

if abs(theta1n ) <80
    Theta1=theta1n;
    Theta2=theta2n;
else 
    Theta1=theta1;
    Theta2=theta2;
end

end