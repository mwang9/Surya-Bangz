%Demo 9
%19-08-2016

%Finding theta
%points = [15 15 95; 160 0 0; 305 175 80];
points = [10 6 5; 6 1 4];
theta=[];
for i=1:size(points,1)%length(points)
    [A,B,C]=Angle4(points(i,1),points(i,2),points(i,3));
    theta(i,1)=A;
    theta(i,2)=B;
    theta(i,3)=C;
end
disp(sprintf('Theta array is'));
disp(theta);

%establish connection to NXT
COM_CloseNXT all;
h = COM_OpenNXT();
COM_SetDefaultNXT(h);

%parameters
mA = NXTMotor('A', 'ActionAtTachoLimit', 'brake');
mB = NXTMotor('B', 'ActionAtTachoLimit', 'Holdbrake');
mC = NXTMotor('C', 'ActionAtTachoLimit', 'Holdbrake');
m = [mA mB mC];
speed = [20 15 10];
orig = [410 -12 -157];
pos = [410 -12 -157];
default = [0 0 0];

%init_setup
for j = m
    j.Stop('off');
    j.ResetPosition();
end

for i=1:size(theta,1)
    for j=[3 2 1]
        if theta(i,j) >= pos(j)
            m(j).TachoLimit = theta(i,j) - pos(j);
            disp(sprintf('posA movement is %d',m(j).TachoLimit));
            m(j).Power = speed(j);
        else theta(i,j) < pos(j)
            m(j).TachoLimit = pos(j) - theta(i,j);
            disp(sprintf('posA movement is %d',-m(j).TachoLimit));
            m(j).Power = -speed(j);
        end
        
        if m(j).TachoLimit ~= 0
            m(j).SendToNXT();
            m(j).WaitFor();
        end
        data = m(j).ReadFromNXT(); %gives matrix of data
        pos(j) = data.Position + orig(j);
        
        while pos(j) ~= theta(i,j)
            error = theta(i,j) - pos(j);
            disp(sprintf('Error is %d',error));
            if abs(error) <= 2
                break;
            end
            m(j).TachoLimit = abs(error);
            m(j).Power = sign(error)*speed(j);
            if abs(error) < 10
                m(j).Power = 4*m(j).Power;
            end
            m(j).SendToNXT();
            m(j).WaitFor();
            data = m(j).ReadFromNXT();
            pos(j) = data.Position + orig(j);
        end
        disp(pos);
    end
end

COM_CloseNXT all;
COM_OpenNXT();
COM_CloseNXT all;