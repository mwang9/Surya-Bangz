%Demo 3
%Combined with kino.m by Rhys


function [theta1, angle2, angle3] = Demo3(x,y,z)


origin_angle2=0;
origin_angle3=0;
Y=6.5; %distance from board (mm)
L1=12; %length between motor 2 and 3
L2=22.5; %length between motor 3 and pivot
%L3=50; %pen length
h1=20; 
l1=0;
r1=4; %radius of motor gear
r2=5.6; %radius of arm gear
theta1=round(rad2deg((r2/r1)*atan2(x,(y+Y))))
%dtheta1=theta1-angle1
M=sqrt(x^2+(y+Y)^2);
syms theta2 theta3;
%S=solve(M==L1*cos(theta2)+L2*cos(theta3), z==L1*sin(theta2)+L2*sin(theta3)-L3)
%S.theta2
%S
[theta2,theta3]=solve(M==l1+L1*cos(theta2)+L2*cos(theta3+theta2), z==L1*sin(theta2)+L2*sin(theta3+theta2)+h1);
theta2_1=eval(theta2(1));
theta3_1=eval(theta3(1));
theta2_2=eval(theta2(2));
theta3_2=eval(theta3(2));
%if abs(theta2_1) < (pi/2*8/9)
   angle21=round(rad2deg(theta2_1-origin_angle2))
    angle31=round((rad2deg(theta3_1-origin_angle3)))
    
  
%else
        disp(sprintf('if2'))
    angle22=round(rad2deg(theta2_2-origin_angle2))
    angle32=round((rad2deg(theta3_2-origin_angle3)))

    %dtheta2=theta2_2-angle2
   % angle2=theta2_2
   % dtheta3=theta3_2-angle3
    %angle3=theta3_2
%end



end
