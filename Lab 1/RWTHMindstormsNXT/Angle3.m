function [theta3,theta1,theta2] = Angles(x,y,z)

%startup_rvc
mdl_twolink; %activate twolink-robot-arm

angleB  = 0; %starter angle motor B
angleC  = 0; %starter angle motor C
L1      = 12.0;      %length arm B-C
L2      = 22.5;      %length arm B-tip
d1      = 0;
d2      = 0;
alpha1  = 0;
alpha2  = 0;


xend = x;   %endposition in x
yend = y;   %endposition in y
zend = z;

ratioBC=40/24;         %ratio of gears


L(1)=Link([angleB d1 L1 alpha1]); %setting robot properties
L(2)=Link([angleC d2 L2 alpha2]); %setting robot properties

two_link = SerialLink(L, 'name', 'two link');    %displaying robot properties

links = two_link.links;


x_offset = -16;     %horizontal offset of Motor B and C from area
y_offset = -8.5;

yn = yend-y_offset;
xn = sqrt((xend+x_offset)^2+yn^2);


z_offset = -20;      %vertical offset of motor B and C from floor
zn = zend + z_offset;

T = transl(xn,zn,0);  %end position of pen tip

q = two_link.ikine(T,[0 0], [1 1 0 0 0 0]);

theta1 = -q(1);                     %just for plot
theta2 = ratioBC*(-q(2))+theta1;    %just for plot

%theta1 = 0;        for testing
%theta2 = 0;

%two_link.plot([theta1 theta2])

r1 = 40; %radius of motor gear
r2 = 56; %radius of arm gear

theta3 = round(r2/r1*atan2d(xend-x_offset,yn));    %angle of ground motor
theta1 = round(-q(1)/pi*180);   %convert into degrees
theta2 = round(q(2)/pi*180);   %convert into degrees


end