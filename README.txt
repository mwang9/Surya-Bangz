METR4202 Advanced Control and Robotics 
by Team SURYA-BANGZ

REQUIREMENTS
Each lab has several required components and installations before they can be tested/operated.

Lab 1
- Lego Mindstorms NXT Motors
- NXT Fantom Driver
https://www.lego.com/en-us/mindstorms/downloads
- RWTH Mindstorms NXT Toolbox
http://www.mindstorms.rwth-aachen.de/trac/wiki/Download
- USB Driver		https://sourceforge.net/projects/libusb-win32/files/libusb-win32-releases/1.2.6.0/

Lab 2
- Microsoft Lifecam
- LifeCam Windows View/Control Application
http://robotics.itee.uq.edu.au/~metr4202/software/StudioFW1033.exe
- LifeCam Windows Drivers (v.3.6)
http://robotics.itee.uq.edu.au/~metr4202/software/LifeCam3.60.exe
- USBWebCams Driver
https://au.mathworks.com/hardware-support/matlab-webcam.html

Lab 3
- Dynamixel AX-18F and MX-12W
- Dynamixel SDK for Windows
http://support.robotis.com/en/software/dynamixel_sdk/usb2dynamixel/usb2dxl_windows.htm

INSTALLATION
The required installations are dependent on the devices to be operated.

NXTs
1. Run the NXT Fantom Driver setup.exe for your OS
	- also run the USB Driver if necessary
2. Ensure 'Lab1'/RWTHMindStormsNXT is added to the MATLAB path
	- this is done by clicking 'Set Path' > 'Add folder'

Microsoft Lifecam
1. Follow the intructions indicated in 'Lab 2'/'Microsoft Lifecam Chimera.pdf'

Dynamixels
1. Plug in the Dynamixel USB adaptor
2. Ensure 'Lab 3'/dxl_sdk_win... is added to the MATLAB path

LICENSE/COPYRIGHT
This repository is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the RWTH - Mindstorms NXT Toolbox and Astar library and their respective distribution licenses.